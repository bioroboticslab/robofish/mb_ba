
#include <robofish/interfaces/IBody.h>
#include <robofish/mb_ba/Raycaster.h++>
#include <robofish/mb_ba/common.h++>

namespace robofish::mb_ba
{
	Raycaster::Raycaster(stx::span<float const, 2>           world,
	                     float                               far_plane,
	                     std::optional<std::array<float, 2>> view_of_agents,
	                     std::optional<std::array<float, 2>> view_of_walls)
	: world_rect{{0, 0}, {world[0], world[1]}, 0}
	, far_plane(far_plane)
	{
		auto view_of_agents_size = std::size_t{0};
		if (view_of_agents) {
			auto const [fop, num]  = *view_of_agents;
			auto const half        = fop * std::acos(-1) / 360.f;
			view_of_agents_sectors = linspace(-half, half, num + 1);
			view_of_agents_size    = num;
		}

		auto view_of_walls_size = std::size_t{0};
		if (view_of_walls) {
			auto const [fop, num] = *view_of_walls;
			auto const half       = fop * std::acos(-1) / 360.f;
			view_of_walls_rays    = linspace(-half, half, num);
			view_of_walls_size    = num;
		}

		view_of_agents_m = std::vector<float>(view_of_agents_size);
		view_of_walls_m  = std::vector<float>(view_of_walls_size);
	}

	void Raycaster::operator()(interfaces::IBody const&                            observer,
	                           stx::span<std::shared_ptr<interfaces::IBody> const> bodies)
	{
		centroid_distances(view_of_agents_m, observer, bodies, view_of_agents_sectors);
		for (decltype(auto) val : view_of_agents_m)
			val = intensity_linear(val, far_plane);

		for (std::size_t i = 0; i < std::min(view_of_walls_rays.size(), view_of_walls_m.size()); ++i)
			view_of_walls_m[i] = wall_distance(observer, world_rect, view_of_walls_rays[i]);
		for (decltype(auto) val : view_of_walls_m)
			val = intensity_linear(val, far_plane);
	}

	std::vector<float> const& Raycaster::view_of_agents() const
	{
		return view_of_agents_m;
	}
	std::vector<float> const& Raycaster::view_of_walls() const
	{
		return view_of_walls_m;
	}
}
