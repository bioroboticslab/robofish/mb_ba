#include <robofish/mb_ba/hdf5.h++>

#include <string>
#include <vector>

#include <stdexcept>

#if HAS_MXNET
#include <robofish/mb_ba/mxnet.h++>
#endif

namespace robofish::mb_ba::hdf5
{
	std::vector<hsize_t> get_dimensions(H5::AbstractDs const& data)
	{
		auto dataspace = data.getSpace();

		auto rank  = dataspace.getSimpleExtentNdims();
		auto shape = std::vector<hsize_t>(rank);
		dataspace.getSimpleExtentDims(&shape[0]);
		return shape;
	}

	template<>
	std::string to<std::string>(H5::Attribute const& attribute)
	{
		std::string s;
		switch (attribute.getTypeClass()) {
		case H5T_STRING:
			attribute.read(attribute.getStrType(), s);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return s;
	}

	template<>
	std::string to<std::string>(H5::DataSet const& dataset)
	{
		std::string s;
		switch (dataset.getTypeClass()) {
		case H5T_STRING:
			dataset.read(s, dataset.getStrType());
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return s;
	}

	template<>
	float to<float>(H5::Attribute const& attribute)
	{
		auto dimensions = get_dimensions(attribute);
		if (dimensions.size() > 1 || dimensions.size() == 1 && dimensions[0] != 1) {
			throw std::invalid_argument("Unsupported shape");
		}
		float data;
		switch (attribute.getTypeClass()) {
		case H5T_INTEGER:
			attribute.read(H5::PredType::NATIVE_FLOAT, &data);
			break;
		case H5T_FLOAT:
			attribute.read(H5::PredType::NATIVE_FLOAT, &data);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return data;
	}

	template<>
	std::uint32_t to<std::uint32_t>(H5::Attribute const& attribute)
	{
		auto dimensions = get_dimensions(attribute);
		if (dimensions.size() > 1 || dimensions.size() == 1 && dimensions[0] != 1) {
			throw std::invalid_argument("Unsupported shape");
		}
		std::uint32_t data;

		switch (attribute.getTypeClass()) {
		case H5T_INTEGER:
			attribute.read(H5::PredType::NATIVE_UINT32, &data);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return data;
	}

	template<>
	uint64_t to<uint64_t>(H5::Attribute const& attribute)
	{
		auto dimensions = get_dimensions(attribute);
		if (dimensions.size() > 1 || dimensions.size() == 1 && dimensions[0] != 1) {
			throw std::invalid_argument("Unsupported shape");
		}
		uint64_t data;

		switch (attribute.getTypeClass()) {
		case H5T_INTEGER:
			attribute.read(H5::PredType::NATIVE_UINT64, &data);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return data;
	}

	template<>
	std::vector<float> to<std::vector<float>>(H5::Attribute const& attribute)
	{
		auto dimensions = get_dimensions(attribute);
		if (dimensions.size() != 1) {
			throw std::invalid_argument("Unsupported shape");
		}

		auto data = std::vector<float>(dimensions[0]);

		switch (attribute.getTypeClass()) {
		case H5T_INTEGER:
			attribute.read(H5::PredType::NATIVE_FLOAT, &data[0]);
			break;
		case H5T_FLOAT:
			attribute.read(H5::PredType::NATIVE_FLOAT, &data[0]);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return data;
	}

	template<>
	std::vector<float> to<std::vector<float>>(H5::DataSet const& dataset)
	{
		auto dimensions = get_dimensions(dataset);
		if (dimensions.size() != 1) {
			throw std::invalid_argument("Unsupported shape");
		}

		auto data = std::vector<float>(dimensions[0]);

		switch (dataset.getTypeClass()) {
		case H5T_INTEGER:
			dataset.read(&data[0], H5::PredType::NATIVE_FLOAT);
			break;
		case H5T_FLOAT:
			dataset.read(&data[0], H5::PredType::NATIVE_FLOAT);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return data;
	}

#if HAS_MXNET
	template<>
	mxnet::cpp::NDArray to<mxnet::cpp::NDArray>(H5::Attribute const& attribute)
	{
		auto dimensions = get_dimensions(attribute);

		auto shape = mxnet::cpp::Shape{};
		shape.CopyFrom(dimensions.begin(), dimensions.end());

		auto  ndarray = mxnet::cpp::NDArray(shape, mxnet::cpp::Context::cpu(), false);
		void* data;
		MXNDArrayGetData(ndarray.GetHandle(), &data);

		switch (attribute.getTypeClass()) {
		case H5T_INTEGER:
			attribute.read(H5::PredType::NATIVE_FLOAT, data);
			break;
		case H5T_FLOAT:
			attribute.read(H5::PredType::NATIVE_FLOAT, data);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return ndarray;
	}

	template<>
	mxnet::cpp::NDArray to<mxnet::cpp::NDArray>(H5::DataSet const& dataset)
	{
		auto dimensions = get_dimensions(dataset);

		auto shape = mxnet::cpp::Shape{};
		shape.CopyFrom(dimensions.begin(), dimensions.end());

		auto  ndarray = mxnet::cpp::NDArray(shape, mxnet::cpp::Context::cpu(), false);
		void* data;
		MXNDArrayGetData(ndarray.GetHandle(), &data);

		switch (dataset.getTypeClass()) {
		case H5T_INTEGER:
			dataset.read(data, H5::PredType::NATIVE_FLOAT);
			break;
		case H5T_FLOAT:
			dataset.read(data, H5::PredType::NATIVE_FLOAT);
			break;
		default:
			throw std::invalid_argument("Unsupported HDF5 type class");
		}

		return ndarray;
	}

	template<>
	std::map<std::string, mxnet::cpp::NDArray> to<std::map<std::string, mxnet::cpp::NDArray>>(H5::Group& group)
	{
		std::vector<std::string> names;
		auto                     it_ctx = std::make_tuple(&group, &names);
		group.iterateElems(
		   ".",
		   nullptr,
		   []([[maybe_unused]] hid_t group_id, char const* name, void* data) {
			   auto [group, params] = *static_cast<decltype(it_ctx)*>(data);

			   H5G_stat_t info;
			   group->getObjinfo(name, true, info);
			   switch (info.type) {
			   case H5G_DATASET:
				   params->emplace_back(name);
				   break;
			   default:
				   break;
			   }
			   return 0;
		   },
		   &it_ctx);

		auto ndarrays = std::map<std::string, mxnet::cpp::NDArray>{};
		for (auto& name : names) {
			ndarrays[name] = to<mxnet::cpp::NDArray>(group.openDataSet(name));
		}

		return ndarrays;
	}
#endif
}
