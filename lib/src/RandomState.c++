#include <robofish/mb_ba/RandomState.h++>

#include <array>

#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/seed_seq.hpp>

namespace robofish::mb_ba
{
	template<typename Engine, typename SeedEngine>
	auto generate_seed_seq(SeedEngine& seed_engine)
	{
		auto constexpr state_size = Engine::state_size;
		using type                = typename Engine::result_type;

		std::array<type, state_size>                  seed_data;
		boost::random::uniform_int_distribution<type> seed_distribution;
		auto                                          seed_generator = [&]() { return seed_distribution(seed_engine); };
		std::generate_n(seed_data.data(), seed_data.size(), seed_generator);

		return boost::random::seed_seq(seed_data);
	}

	RandomState::RandomState()
	{
		boost::random::random_device rd;
		engine.seed(rd);
	}

	RandomState::RandomState(RandomState& seed)
	{
		auto seed_seq = generate_seed_seq<decltype(engine)>(seed.engine);
		engine.seed(seed_seq);
	}

	RandomState::RandomState(RandomState const& other)
	: engine(other.engine)
	, normal_distribution(other.normal_distribution)
	{
	}

	RandomState& RandomState::operator=(RandomState const& other)
	{
		engine              = other.engine;
		normal_distribution = other.normal_distribution;
		return *this;
	}

	RandomState::RandomState(std::uint32_t seed)
	{
		auto seed_seq = boost::random::seed_seq({seed});
		engine.seed(seed_seq);
	}

	void RandomState::seed(RandomState& seed)
	{
		engine.seed(seed.engine);
	}

	auto RandomState::uniform(float low, float high) -> float
	{
		return boost::random::uniform_real_distribution<float>(low, high)(engine);
	}

	auto RandomState::uniform(int low, int high) -> int
	{
		return boost::random::uniform_int_distribution<int>(low, high)(engine);
	}

	auto RandomState::normal(float location, float scale) -> float
	{
		return normal_distribution(engine) * scale + location;
	}

	auto RandomState::multivariate_normal(
	   Eigen::Ref<const Eigen::VectorXf>                                                       location,
	   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance)
	   -> Eigen::VectorXf
	{
		auto transform = [&]() -> Eigen::MatrixXf {
			auto chol_solver = Eigen::LLT<Eigen::MatrixXf>(covariance);
			if (chol_solver.info() == Eigen::Success)
				return chol_solver.matrixL();

			auto eigen_solver = Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf>(covariance);
			return eigen_solver.eigenvectors() * eigen_solver.eigenvalues().cwiseSqrt().asDiagonal();
		}();

		return transform * Eigen::VectorXf::NullaryExpr(location.size(), [&]() { return normal_distribution(engine); })
		       + location;
	}

	auto RandomState::multivariate_normal(
	   Eigen::Ref<const Eigen::VectorXf>                                                       location,
	   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance,
	   size_t size) -> Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
	{
		auto transform = [&]() -> Eigen::MatrixXf {
			auto chol_solver = Eigen::LLT<Eigen::MatrixXf>(covariance);
			if (chol_solver.info() == Eigen::Success)
				return chol_solver.matrixL();

			auto eigen_solver = Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf>(covariance);
			return eigen_solver.eigenvectors() * eigen_solver.eigenvalues().cwiseSqrt().asDiagonal();
		}();

		Eigen::MatrixXf samples
		   = (transform
		      * Eigen::MatrixXf::NullaryExpr(location.size(), size, [&]() { return normal_distribution(engine); }))
		        .colwise()
		     + location;
		samples.transposeInPlace();
		return samples;
	}
}
