#include <robofish/interfaces/IBody.h>
#include <robofish/mb_ba/ArrayBody.h++>
#include <robofish/mb_ba/common.h++>

namespace robofish::mb_ba
{
	ArrayBody::ArrayBody(int uid, stx::span<float const, 2> position, stx::span<float const, 2> orientation)
	: IBody(uid)
	, pose_m{position[0], position[1], orientation[0], orientation[1]}
	{
	}

	ArrayBody::ArrayBody(interfaces::IBody const& other)
	: IBody(other.uid)
	, pose_m{other.position().x, other.position().y, other.orientation()[0], other.orientation()[1]}
	{
	}

	ArrayBody::ArrayBody(ArrayBody const& other)
	: IBody(other.uid)
	, pose_m{other.position().x, other.position().y, other.orientation()[0], other.orientation()[1]}
	{
	}

	void ArrayBody::set_pose(stx::span<float const, 4> pose)
	{
		pose_m = {pose[0], pose[1], pose[2], pose[3]};
	}

	void ArrayBody::set_pose(float x, float y, float dx, float dy)
	{
		pose_m = {x, y, dx, dy};
	}

	void ArrayBody::set_position(stx::span<float const, 2> position)
	{
		pose_m[0] = position[0];
		pose_m[1] = position[1];
	}

	void ArrayBody::set_orientation(stx::span<float const, 2> orientation)
	{
		pose_m[2] = orientation[0];
		pose_m[3] = orientation[1];
	}

	auto ArrayBody::pose() const -> stx::span<float const, 4>
	{
		return {pose_m};
	}

	auto ArrayBody::position() const -> cv::Point2f
	{
		return {pose_m[0], pose_m[1]};
	}

	auto ArrayBody::orientation() const -> cv::Vec2f
	{
		return {pose_m[2], pose_m[3]};
	}

	auto ArrayBody::clone() const -> ArrayBody
	{
		return {uid_m, {&pose_m[0], 2}, {&pose_m[2], 2}};
	}
}
