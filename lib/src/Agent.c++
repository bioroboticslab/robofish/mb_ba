#include <robofish/mb_ba/Agent.h++>

namespace robofish::mb_ba
{
	Agent::Agent(stx::span<float const, 2> world,
	             int                       uid,
	             stx::span<float const, 2> position,
	             stx::span<float const, 2> orientation)
	: ArrayBody{uid, position, orientation}
	, world_m{world[0], world[1]}
	{
	}

	Agent::~Agent()
	{
	}

	auto Agent::supported_time_steps() const -> std::vector<std::uint32_t>
	{
		return {};
	}

	void Agent::reset()
	{
	}

	auto Agent::world() const -> stx::span<float const, 2>
	{
		return world_m;
	}

	void move_turn_speed(Agent& agent, stx::span<float const, 2> locomotion, bool force_correct)
	{
		auto turn    = locomotion[0];
		auto forward = locomotion[1];

		decltype(auto) pose = agent.pose();

		auto pos = Point{pose[0], pose[1]};
		auto ori = Vector{pose[2], pose[3]};

		ori           = rotate2D(ori, turn);
		auto pos_cand = pos + ori * locomotion[1];

		if (!contains(agent.world(), pos_cand)) {
			if (force_correct) {
				if (is_zero(turn))
					turn = 0.0175; // Rotate at least by approximately one degree
				auto max_tries = static_cast<int>(std::ceil(CGAL_PI / std::abs(turn)));

				for (auto i = 0; i < max_tries; ++i) {
					ori      = rotate2D(ori, turn);
					pos_cand = pos + ori * forward;
					if (contains(agent.world(), pos_cand)) {
						std::cerr << "Agent leaving world prevented by force "
						             "correcting turn angle"
						          << std::endl;
						goto apply_locomotion;
					}
				}
			}

			std::cerr << "Agent leaving world prevented by ceasing locomotion" << std::endl;
			return;
		}

	apply_locomotion:
		pos = pos_cand;

		auto       ori_norm = Direction{ori};
		std::array new_pose = {pos.x(), pos.y(), ori_norm.dx(), ori_norm.dy()};
		agent.set_pose(new_pose);
	}
}
