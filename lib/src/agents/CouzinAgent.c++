#include <robofish/interfaces/IBody.h>
#include <robofish/mb_ba/agents/CouzinAgent.h++>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace robofish::mb_ba
{
	template<typename Vec2_1, typename Vec4, typename Vec2_2>
	static auto zob_target(Vec2_1& target, float zob, Vec4 const& pose, Vec2_2 const& world)
	{
		bool repulsed = false;
		target        = Vec2_1{0, 0};

		if (world[0] - pose[0] < zob) {
			repulsed = true;
			target += Vec2_1{-1, 0};
		}

		if (world[1] - pose[1] < zob) {
			repulsed = true;
			target += Vec2_1{0, -1};
		}

		if (pose[0] < zob) {
			repulsed = true;
			target += Vec2_1{1, 0};
		}

		if (pose[1] < zob) {
			repulsed = true;
			target += Vec2_1{0, 1};
		}

		return repulsed;
	}

	CouzinAgent::CouzinAgent(stx::span<float const, 2> world,
	                         int                       uid,
	                         stx::span<float const, 2> position,
	                         stx::span<float const, 2> orientation,
	                         CouzinAgent::Config       config)
	: Agent{world, uid, position, orientation}
	, zor(config.zor)
	, zoo(config.zoo)
	, zoa(config.zoa)
	, zob(config.zob)
	, rr(config.zor)
	, ro(config.zor + config.zoo)
	, ra(config.zor + config.zoo + config.zoa)
	, fop(config.fop)
	, tr(config.tr)
	, s(config.s)
	, sd(config.sd)
	{
	}

	void CouzinAgent::tick(stx::span<std::shared_ptr<interfaces::IBody> const> bodies, std::uint32_t time_step)
	{
		auto const ts       = static_cast<float>(time_step) / 1000.f;
		bool       repulsed = false;
		auto       d_r      = Vector{0, 0};
		auto       d_o      = Vector{0, 0};
		auto       d_a      = Vector{0, 0};

		repulsed = zob_target(d_r, zob, pose_m, world_m);

		if (!repulsed)
			for (auto const& body : bodies) {
				if (uid == body->uid) {
					continue;
				}

				auto towards  = Vector{Point{pose_m[0], pose_m[1]}, Point{body->position().x, body->position().y}};
				auto distance = std::sqrt(towards.squared_length());
				towards /= distance;
				if (distance < rr) {
					repulsed = true;
					d_r -= towards;
				} else {
					if (repulsed) {
						continue;
					}
					if (dot2D(orientation(), towards) > fop / 2.f) {
						continue;
					}

					if (distance < ro) {
						d_o = d_o + body->orientation();
					} else if (distance < this->ra) {
						d_a += towards;
					}
				}
			}

		auto d = Vector{0, 0};

		if (repulsed) {
			d = d_r;
		} else {
			d = d_o + d_a;
		}

		if (is_zero(d[0]) && is_zero(d[1])) {
			d = {orientation()[0], orientation()[1]};
		}

		d = rotate2D(d, random.normal(0, sd));

		auto max_turn_angle      = ts * tr;
		auto required_turn_angle = angle2D(orientation(), d);
		auto turn_angle          = std::min(std::abs(required_turn_angle), max_turn_angle);

		if (required_turn_angle < 0) {
			turn_angle *= -1;
		}

		locomotion_m[0] = turn_angle;
		locomotion_m[1] = s * ts;
		move_turn_speed(*this, locomotion_m, true);
	}

	auto CouzinAgent::locomotion() const -> stx::span<float const>
	{
		return {locomotion_m};
	}

	auto CouzinAgent::Config::load(std::string const& filename) -> CouzinAgent::Config
	{
		using namespace boost::property_tree;

		ptree tree;
		read_xml(filename, tree, xml_parser::trim_whitespace | xml_parser::no_comments);

		auto zor = tree.get<float>("couzin.zones.repulsion");
		auto zoo = tree.get<float>("couzin.zones.orientation");
		auto zoa = tree.get<float>("couzin.zones.attraction");
		auto zob = tree.get<float>("couzin.zones.wallrepulsion");

		auto to_radian = [](float angle, std::string const& unit) {
			if (unit == "radian") {
				return angle;
			} else if (unit == "degree") {
				return angle * std::acos(-1.f) / 180.f;
			} else {
				throw std::invalid_argument("Angle unit must be radian or degree");
			}
		};

		auto fop = to_radian(tree.get<float>("couzin.field_of_perception"),
		                     tree.get<std::string>("couzin.field_of_perception.<xmlattr>.unit"));

		auto tr = to_radian(tree.get<float>("couzin.turn_rate"),
		                    tree.get<std::string>("couzin.turn_rate.<xmlattr>.unit"));

		auto s  = tree.get<float>("couzin.speed");
		auto sd = to_radian(tree.get<float>("couzin.direction_error"),
		                    tree.get<std::string>("couzin.direction_error.<xmlattr>.unit"));

		return {zor, zoo, zoa, zob, fop, tr, s, sd};
	}

	void CouzinAgent::seed(RandomState& seed)
	{
		random.seed(seed);
	}
}
