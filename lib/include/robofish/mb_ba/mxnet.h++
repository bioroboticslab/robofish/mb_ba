#pragma once

#if HAS_MXNET

#include <limits>
#include <regex>
#include <algorithm>
#include <mxnet-cpp/MxNetCpp.h>

namespace mxnet::cpp
{
	inline bool operator==(const Context& lhs, const Context& rhs)
	{
		return lhs.GetDeviceType() == rhs.GetDeviceType() && lhs.GetDeviceId() == rhs.GetDeviceId();
	}
	inline bool operator!=(const Context& lhs, const Context& rhs)
	{
		return !(lhs == rhs);
	}

	inline bool on_cpu(NDArray const& ndarray)
	{
		switch (ndarray.GetContext().GetDeviceType()) {
		case kCPU:
			return true;
		case kCPUPinned:
			return true;
		default:
			return false;
		}
	}

	inline void InferShapePartial(Symbol const&                                      symbol,
	                              const std::map<std::string, std::vector<mx_uint>>& arg_shapes,
	                              std::vector<std::vector<mx_uint>>*                 in_shape,
	                              std::vector<std::vector<mx_uint>>*                 aux_shape,
	                              std::vector<std::vector<mx_uint>>*                 out_shape)
	{

		std::vector<const char*> keys;
		std::vector<mx_uint>     arg_ind_ptr;
		std::vector<mx_uint>     arg_shape_data;

		for (const auto& arg : arg_shapes) {
			keys.push_back(arg.first.c_str());
			arg_ind_ptr.push_back(arg_shape_data.size());
			for (auto i : arg.second) {
				arg_shape_data.push_back(i);
			}
		}
		arg_ind_ptr.push_back(arg_shape_data.size());

		mx_uint         in_shape_size;
		const mx_uint*  in_shape_ndim;
		const mx_uint** in_shape_data;
		mx_uint         out_shape_size;
		const mx_uint*  out_shape_ndim;
		const mx_uint** out_shape_data;
		mx_uint         aux_shape_size;
		const mx_uint*  aux_shape_ndim;
		const mx_uint** aux_shape_data;
		int             complete;

		CHECK_EQ(MXSymbolInferShapePartial(symbol.GetHandle(),
		                                   keys.size(),
		                                   keys.data(),
		                                   arg_ind_ptr.data(),
		                                   arg_shape_data.data(),
		                                   &in_shape_size,
		                                   &in_shape_ndim,
		                                   &in_shape_data,
		                                   &out_shape_size,
		                                   &out_shape_ndim,
		                                   &out_shape_data,
		                                   &aux_shape_size,
		                                   &aux_shape_ndim,
		                                   &aux_shape_data,
		                                   &complete),
		         0);

		if (complete) {
			for (mx_uint i = 0; i < in_shape_size; ++i) {
				in_shape->push_back(std::vector<mx_uint>());
				for (mx_uint j = 0; j < in_shape_ndim[i]; ++j) {
					(*in_shape)[i].push_back(in_shape_data[i][j]);
				}
			}
			for (mx_uint i = 0; i < aux_shape_size; ++i) {
				aux_shape->push_back(std::vector<mx_uint>());
				for (mx_uint j = 0; j < aux_shape_ndim[i]; ++j) {
					(*aux_shape)[i].push_back(aux_shape_data[i][j]);
				}
			}
			for (mx_uint i = 0; i < out_shape_size; ++i) {
				out_shape->push_back(std::vector<mx_uint>());
				for (mx_uint j = 0; j < out_shape_ndim[i]; ++j) {
					(*out_shape)[i].push_back(out_shape_data[i][j]);
				}
			}
		}
	}

	inline bool IsRecurrentState(std::string const& name)
	{
		return std::regex_match(name, std::regex{"^(.*_)?state[0-9]+$", std::regex::extended});
	}

	inline std::vector<std::string> ListRecurrentStates(Symbol const& symbol)
	{
		std::vector<std::string> result;

		auto const arg_names = symbol.ListArguments();
		auto const aux_names = symbol.ListAuxiliaryStates();
		auto const out_names = symbol.ListOutputs();

		std::copy_if(arg_names.begin(), arg_names.end(), std::back_inserter(result), &IsRecurrentState);

		return result;
	}

	inline void InferRecurrentStates(Symbol const&                         symbol,
	                                 const Context&                        context,
	                                 std::map<std::string, NDArray>*       args_map,
	                                 const std::map<std::string, NDArray>& known_args)
	{
		const auto                                  arg_name_list = symbol.ListArguments();
		std::vector<std::vector<mx_uint>>           in_shapes, aux_shapes, out_shapes;
		std::map<std::string, std::vector<mx_uint>> arg_shapes;

		for (const auto& arg_name : arg_name_list) {
			auto iter = known_args.find(arg_name);
			if (iter != known_args.end()) {
				arg_shapes[arg_name] = iter->second.GetShape();
			}
		}

		InferShapePartial(symbol, arg_shapes, &in_shapes, &aux_shapes, &out_shapes);

		auto state_name_list = ListRecurrentStates(symbol);
		auto out_name_list   = symbol.ListOutputs();
		auto state_offset    = out_name_list.size() - state_name_list.size();
		for (size_t i = 0; i < state_name_list.size(); ++i) {
			const auto& shape      = out_shapes[state_offset + i];
			const auto& state_name = state_name_list[i];
			if (shape.size() != 0) {
				(*args_map)[state_name] = NDArray(shape, context, false);
				Zero()(state_name, &(*args_map)[state_name]);
			}
		}
	}
}

#endif
