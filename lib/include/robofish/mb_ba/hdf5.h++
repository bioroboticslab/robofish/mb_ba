#pragma once

#if HAS_HDF5

#include <H5Cpp.h>

namespace robofish::mb_ba::hdf5
{
	template<typename T>
	T to(H5::Attribute const& attribute);
	template<typename T>
	T to(H5::DataSet const& attribute);
	template<typename T>
	T to(H5::Group& group);
}

#endif
