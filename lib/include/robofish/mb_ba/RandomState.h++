#pragma once

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <iterator>
#include <robofish/mb_ba/span.h++>

#include <Eigen/Dense>

namespace robofish::mb_ba
{
	struct RandomState final
	{
	private:
		boost::random::mt19937                    engine;
		boost::random::normal_distribution<float> normal_distribution;

	public:
		RandomState();
		RandomState(RandomState& seed);
		RandomState(RandomState const& other);
		RandomState& operator=(RandomState const& other);
		RandomState(std::uint32_t seed);

		void seed(RandomState& seed);

		auto uniform(float low, float high) -> float;

		auto uniform(int low, int high) -> int;

		auto normal(float location, float scale) -> float;

		auto multivariate_normal(
		   Eigen::Ref<const Eigen::VectorXf>                                                       location,
		   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance)
		   -> Eigen::VectorXf;
		auto multivariate_normal(
		   Eigen::Ref<const Eigen::VectorXf>                                                       location,
		   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance,
		   size_t size) -> Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

		template<typename Range>
		auto multinomial(Range&& probabilities) -> int;
	};

	template<typename Range>
	auto RandomState::multinomial(Range&& probabilities) -> int
	{
		return boost::random::discrete_distribution<int>(probabilities)(engine);
	}
}
