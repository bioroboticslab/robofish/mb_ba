#pragma once

#include <array>
#include <memory>

#include <CGAL/Cartesian.h>

#include <robofish/mb_ba/common.h++>
#include <robofish/mb_ba/ArrayBody.h++>

namespace robofish::mb_ba
{
	class Agent : public ArrayBody
	{
	protected:
		std::array<float, 2> world_m;

	public:
		Agent(stx::span<float const, 2> world,
		      int                       uid,
		      stx::span<float const, 2> position,
		      stx::span<float const, 2> orientation);
		virtual ~Agent();

		virtual auto supported_time_steps() const -> std::vector<std::uint32_t>;

		virtual void reset();
		virtual void tick(stx::span<std::shared_ptr<interfaces::IBody> const> bodies, std::uint32_t time_step) = 0;

		virtual auto locomotion() const -> stx::span<float const> = 0;

		auto world() const -> stx::span<float const, 2>;
	};

	void move_turn_speed(Agent& agent, stx::span<float const, 2> locomotion, bool force_correct);
}
