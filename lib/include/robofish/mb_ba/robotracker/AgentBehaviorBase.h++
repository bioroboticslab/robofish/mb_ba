#pragma once

#include <robofish/interfaces/IBehavior.h>

namespace robofish::mb_ba
{
	class AgentBehaviorBase : public interfaces::IBehavior
	{
		Q_OBJECT

	public:
		virtual void setConfig(QString const& filename) = 0;
	};
}
