#pragma once

#include <memory>
#include <vector>

#include <QLayout>
#include <QLayoutItem>

#include <robofish/interfaces/IBody.h>

namespace robofish::mb_ba
{
	inline void removeAll(QLayout* parent)
	{
		QLayoutItem* item;
		while ((item = parent->takeAt(0)) != nullptr) {
			if (auto layout = item->layout(); layout) {
				removeAll(layout);
			} else if (auto widget = item->widget(); widget) {
				delete widget;
			}
			delete item;
		}
	}

	inline auto collect(int uid, BodyList robots, BodyList fish)
	   -> std::tuple<SharedBody, std::vector<std::shared_ptr<interfaces::IBody>>>
	{
		std::shared_ptr<interfaces::IBody>              body;
		std::vector<std::shared_ptr<interfaces::IBody>> others;

		for (auto r : robots) {
			if (r->uid == uid)
				body = std::static_pointer_cast<interfaces::IBody>(r);
			else
				others.push_back(r);
		}

		for (auto f : fish)
			others.push_back(f);

		return {body, others};
	}
}
