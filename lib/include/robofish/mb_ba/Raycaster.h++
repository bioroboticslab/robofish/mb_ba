#pragma once

#include <random>
#include <optional>
#include <variant>
#include <tuple>
#include <array>
#include <memory>

#include <robofish/interfaces/IBody.h>
#include <robofish/mb_ba/common.h++>
#include <robofish/mb_ba/span.h++>

namespace robofish::mb_ba
{
	struct Raycaster final
	{
	private:
		Rectangle world_rect;

		float              far_plane;
		std::vector<float> view_of_agents_sectors;
		std::vector<float> view_of_walls_rays;

		std::vector<float> view_of_agents_m;
		std::vector<float> view_of_walls_m;

	public:
		Raycaster(stx::span<float const, 2>           world,
		          float                               far_plane,
		          std::optional<std::array<float, 2>> view_of_agents,
		          std::optional<std::array<float, 2>> view_of_walls);

		void operator()(interfaces::IBody const& observer, stx::span<std::shared_ptr<interfaces::IBody> const> bodies);

		std::vector<float> const& view_of_agents() const;
		std::vector<float> const& view_of_walls() const;
	};
}
