#pragma once

#if HAS_MXNET

#include <optional>
#include <variant>
#include <tuple>
#include <array>
#include <memory>

#include <robofish/mb_ba/Agent.h++>
#include <robofish/mb_ba/mxnet.h++>
#include <robofish/mb_ba/common.h++>
#include <robofish/mb_ba/span.h++>
#include <robofish/mb_ba/RandomState.h++>
#include <robofish/mb_ba/Raycaster.h++>

namespace robofish::mb_ba
{
	class MXNetAgent : public Agent
	{
	public:
		struct Config
		{
			mxnet::cpp::Symbol                           symbols;
			std::map<std::string, mxnet::cpp::NDArray>   params;
			std::uint32_t                                time_step;
			std::array<std::tuple<float, float, int>, 2> locomotion;
			std::optional<std::array<float, 2>>          view_of_agents;
			std::optional<std::array<float, 2>>          view_of_walls;
			float                                        far_plane;

#if HAS_HDF5
			static auto           load(std::string const& filename) -> Config;
			static constexpr auto filename_extension = "hdf5";
#endif
		};

	private:
		Raycaster raycaster;

	protected:
		RandomState random;

		std::uint32_t time_step_;

		std::vector<float> feature_m;
		stx::span<float>   locomotion_m;

		std::vector<std::vector<float>> locomotion_bins;

		mxnet::cpp::Symbol                         symbols_;
		std::map<std::string, mxnet::cpp::NDArray> arguments_;
		std::vector<std::string>                   state_names_;
		std::unique_ptr<mxnet::cpp::Executor>      executor;

	public:
		MXNetAgent(stx::span<float const, 2> world,
		           int                       uid,
		           stx::span<float const, 2> position,
		           stx::span<float const, 2> orientation,
		           Config                    config);

		auto supported_time_steps() const -> std::vector<std::uint32_t> override;

		void reset() override;
		void tick(stx::span<std::shared_ptr<interfaces::IBody> const> bodies, std::uint32_t time_step) override;

		auto locomotion() const -> stx::span<float const> override;
		void set_locomotion(stx::span<float const> locomotion);

		auto state_names() const -> std::vector<std::string> const&;

		auto time_step() const -> std::uint32_t;
		auto arguments() const -> std::map<std::string, mxnet::cpp::NDArray> const&;
		auto symbols() const -> mxnet::cpp::Symbol const&;

		void seed(RandomState& seed);
	};
}

#endif
