#pragma once

#include <QWidget>
#include <QLayout>
#include <QString>
#include <QFileDialog>
#include <QLineEdit>

namespace robofish::mb_ba
{
	class FileSelect : public QWidget
	{
		Q_OBJECT
	protected:
		QLineEdit* selected_file_name;

	public:
		FileSelect(QWidget*             parent          = nullptr,
		           QString const&       caption         = {},
		           QString const&       dir             = {},
		           QString const&       filter          = {},
		           QString*             selected_filter = nullptr,
		           QFileDialog::Options options         = 0);
		void clear();
	signals:
		void fileSelected(QString const& file_name);
	};
}
