#pragma once

#include <limits>
#include <cmath>

#include <CGAL/Cartesian.h>

#include <opencv2/core/matx.hpp>

#include <robofish/interfaces/IBody.h>
#include <robofish/mb_ba/span.h++>

class QFile;

namespace robofish::interfaces
{
	class IBody;
}

namespace robofish::mb_ba
{
	using Point     = CGAL::Cartesian<float>::Point_2;
	using Vector    = CGAL::Cartesian<float>::Vector_2;
	using Ray       = CGAL::Cartesian<float>::Ray_2;
	using Direction = CGAL::Cartesian<float>::Direction_2;
	using Segment   = CGAL::Cartesian<float>::Segment_2;
	using Rectangle = CGAL::Cartesian<float>::Iso_rectangle_2;

	template<typename Vec2_1, typename Vec2_2>
	auto dot2D(Vec2_1 const& a, Vec2_2 const& b)
	{
		return a[0] * b[0] + a[1] * b[1];
	}

	template<typename Vec2_1, typename Vec2_2>
	auto det2D(Vec2_1 const& a, Vec2_2 const& b)
	{
		return a[0] * b[1] - a[1] * b[0];
	}

	template<typename Vec2_1, typename Vec2_2>
	auto angle2D(Vec2_1 const& a, Vec2_2 const& b)
	{
		return std::atan2(det2D(a, b), dot2D(a, b));
	}

	template<typename Vec2>
	Vec2 rotate2D(Vec2 const& vector, float angle)
	{
		return {std::cos(angle) * vector[0] - std::sin(angle) * vector[1],
		        std::sin(angle) * vector[0] + std::cos(angle) * vector[1]};
	}

	template<typename Vec2>
	auto norm(Vec2 const& vector)
	{
		return std::sqrt(vector[0] * vector[0] + vector[1] * vector[1]);
	}

	template<typename Vec2>
	auto normalized(Vec2 const& vector)
	{
		return vector / norm(vector);
	}

	auto operator+(Vector const& a, cv::Vec2f const& b) -> Vector;

	auto contains(Rectangle const& r, Point const& p) -> bool;
	auto contains(stx::span<float const, 2> r, Point const& p) -> bool;

	auto digitize(float value, stx::span<float const> bins) -> std::size_t;

	constexpr auto infinity = std::numeric_limits<float>::infinity();

	auto wall_distance(interfaces::IBody const& observer, Rectangle const& walls, float ray_angle) -> float;

	void centroid_distances(stx::span<float>                                    distances,
	                        interfaces::IBody const&                            observer,
	                        stx::span<std::shared_ptr<interfaces::IBody> const> others,
	                        stx::span<float const>                              sector_limits);

	auto is_zero(float val, float epsilon = 1e-5f) -> bool;
	auto is_zero(Vector v, float epsilon = 1e-5f) -> bool;

	auto linspace(float min, float max, std::size_t n) -> std::vector<float>;

	auto calc_locomotion(stx::span<float const, 4> prev_pose, stx::span<float const, 4> curr_pose)
	   -> std::array<float, 2>;

	auto intensity_linear(float d, float max_d) -> float;
}
