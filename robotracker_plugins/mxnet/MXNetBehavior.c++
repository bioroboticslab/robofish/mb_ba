
#include "MXNetBehavior.h++"

#include <robofish/mb_ba/common.h++>
#include <robofish/mb_ba/hdf5.h++>
#include <robofish/mb_ba/mxnet.h++>

namespace robofish::mb_ba
{
	void MXNetBehavior::setConfig(QString const& filename)
	{
		try {
			AgentBehavior::setConfig(filename);
		} catch (H5::Exception const& e) {
			qWarning() << "Failed to load agent configuration from" << filename;
		} catch (dmlc::Error const& e) {
			qWarning() << MXGetLastError();
			qWarning() << "Failed to load agent configuration from" << filename;
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to load agent configuration from" << filename;
		}
	}

	auto MXNetBehavior::supportedTimesteps() -> std::vector<std::uint32_t>
	{
		return {config->time_step};
	}

	void MXNetBehavior::activate(SharedBody robot, interfaces::IModelWorldDescriptor* world)
	{
		try {
			AgentBehavior::activate(robot, world);
		} catch (H5::Exception const& e) {
			qWarning() << "Failed to instantiate agent";
		} catch (dmlc::Error const& e) {
			qWarning() << MXGetLastError();
			qWarning() << "Failed to instantiate agent";
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to instantiate agent";
		}
	}
}
