
#include "CouzinBehavior.h++"

namespace robofish::mb_ba
{
	void CouzinBehavior::setConfig(QString const& filename)
	{
		try {
			AgentBehavior::setConfig(filename);
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to load agent configuration from" << filename;
		}
	}

	auto CouzinBehavior::supportedTimesteps() -> std::vector<std::uint32_t>
	{
		return {};
	}

	void CouzinBehavior::activate(SharedBody robot, interfaces::IModelWorldDescriptor* world)
	{
		try {
			AgentBehavior::activate(robot, world);
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to instantiate agent";
		}
	}
}
