import sys
import numpy
import cv2
import h5py

from ast import literal_eval
from argparse import ArgumentParser
from copy import deepcopy
from math import floor, pi
from pathlib import Path
from numpy import (
    squeeze,
    expand_dims,
    interp,
    dot,
    arccos,
    clip,
    identity,
    float32,
    empty,
    power,
    where,
)
from numpy.linalg import norm

from scipy.ndimage import convolve1d
from scipy.ndimage.filters import gaussian_filter1d
from scipy.signal import argrelmax, find_peaks


class FishTankRectifier:
    def __init__(self, camera, corners, world):

        self._cam_matrix, self._dist_coeff = [float32(e) for e in camera]
        self._image_size = (2 * int(self._cam_matrix[0][2]), 2 * int(self._cam_matrix[1][2]))
        self._new_cam_matrix, _ = cv2.getOptimalNewCameraMatrix(
            self._cam_matrix, self._dist_coeff, self._image_size, 1, self._image_size
        )

        homography, _ = cv2.findHomography(
            float32([[*point] for point in corners]),
            float32([[0, 0], [0, world[1]], [world[0], world[1]], [world[0], 0]]),
        )
        self._homography = homography.astype(float32)

        self._map1, self._map2 = cv2.initUndistortRectifyMap(
            self._cam_matrix,
            self._dist_coeff,
            identity(3, dtype=float32),
            self._new_cam_matrix,
            self._image_size,
            cv2.CV_16SC2,
        )

    def undistort(self, points):
        points = expand_dims(float32(points), -2)
        undistorted = cv2.undistortPoints(
            points, self._cam_matrix, self._dist_coeff, P=self._new_cam_matrix
        )
        return squeeze(undistorted, -2)

    def px2cm(self, points):
        return float32(
            [
                [point[0][0] / point[2][0], point[1][0] / point[2][0]]
                for point in (
                    cv2.gemm(self._homography, float32([[point[0]], [point[1]], [1]]), 1, None, 0)
                    for point in points
                )
            ]
        )

    def undistort_image(self, image):
        return cv2.remap(image, self._map1, self._map2, cv2.INTER_LINEAR)

    def __call__(self, points):
        return self.px2cm(self.undistort(points))


class NotEnoughMaxima(Exception):
    def __init__(self, curvature):
        super().__init__()
        self.curve = curvature


class FishContour:
    def __init__(self, contour):
        self.c = squeeze(float32(contour))

    def smoothed(self, smooth_sigma=10):
        return FishContour(gaussian_filter1d(self.c, smooth_sigma, mode="wrap", axis=0))

    @property
    def curvature(self):
        x_1, y_1 = convolve1d(self.c, [-0.5, 0.0, 0.5], mode="wrap", axis=0).transpose()
        x_2, y_2 = convolve1d(self.c, [1.0, -2.0, 1.0], mode="wrap", axis=0).transpose()
        return abs(x_1 * y_2 - y_1 * x_2) / power(x_1 * x_1 + y_1 * y_1, 3 / 2)

    def find_maxima(self, at_least_n):
        curvature = abs(self.curvature)
        maxima = argrelmax(curvature, mode="wrap")[0].tolist()
        if len(maxima) < at_least_n:
            raise NotEnoughMaxima(curvature)
        return sorted(maxima, key=lambda x: curvature[x], reverse=True)

    def find_tip_tail(self, smooth_sigma=10):
        smoothed = self.smoothed(smooth_sigma)
        tail, tip = smoothed.find_maxima(2)[:2]
        return self.c[tip, :], self.c[tail, :]

    def find_head(self, head_size=20, smooth_sigma=10):
        tip, _ = self.find_tip_tail(smooth_sigma)
        squared_tip_distance = power(self.c[:, 0] - tip[0], 2) + power(self.c[:, 1] - tip[1], 2)
        near_tip = self.c[where(squared_tip_distance < head_size * head_size)]
        moments = cv2.moments(expand_dims(near_tip, axis=1))
        centroid = moments["m10"] / moments["m00"], moments["m01"] / moments["m00"]
        return tip, float32(centroid)


def run(session_path, camera, corners, world, max_angular_velocity, *, trackset=None):
    session_path = Path(session_path)
    rectifier = FishTankRectifier(camera, corners, world)

    video_object = numpy.load(session_path / "video_object.npy", encoding="latin1").item()

    blobs_path = session_path / "preprocessing" / "blobs_collection_no_gaps.npy"
    if not blobs_path.exists():
        blobs_path = session_path / "preprocessing" / "blobs_collection.npy"
    list_of_blobs = numpy.load(blobs_path, encoding="latin1").item()

    tracking_interval = video_object.tracking_interval
    if tracking_interval is None:
        begin, end = 0, len(list_of_blobs.blobs_in_video) - 1
        while begin <= end and len(list_of_blobs.blobs_in_video[begin]) == 0:
            begin += 1
        while begin <= end and len(list_of_blobs.blobs_in_video[end]) == 0:
            end -= 1
        tracking_interval = begin, end
    elif isinstance(tracking_interval, list):
        tracking_interval = tracking_interval[0]
    else:
        assert False

    num_frames = tracking_interval[1] - tracking_interval[0]
    video_path = Path(video_object.video_path)
    video_name = video_path.name
    time_step = floor(1000.0 / video_object.frames_per_second)
    identities = list(range(1, 1 + video_object.number_of_animals))

    if isinstance(trackset, h5py.Group):
        pass
    elif trackset is None:
        trackset = h5py.File((session_path.parent / video_name).with_suffix(".hdf5"))
    else:
        trackset = h5py.File(str(trackset), "w")

    del video_object

    tracked_blobs = list_of_blobs.blobs_in_video[tracking_interval[0] : tracking_interval[1]]

    precise_poses = {uid: [None for t in range(num_frames)] for uid in identities}

    # Calculate head position and orientation where possible.
    # This requires at least two local maxima to be detected in the curvature
    # derived from the blob's contour.
    for t in range(num_frames):
        for blob in (blob for blob in tracked_blobs[t] if blob.is_an_individual):
            uid = blob.final_identity
            try:
                head = FishContour(blob.contour).find_head()
            except NotEnoughMaxima:
                continue
            tip, centroid = rectifier(head)[:2]
            ori = tip - centroid
            ori = ori / norm(tip - centroid)
            pose = float32([*centroid, *ori])
            precise_poses[uid][t] = pose

    # Invalidate poses with impossible velocities
    for uid in identities:
        assert precise_poses[uid][0] is not None
        assert precise_poses[uid][-1] is not None
        for t in range(1, num_frames - 1):
            curr_pose = precise_poses[uid][t]
            if curr_pose is None:
                continue
            prev_pose = precise_poses[uid][t - 1]
            next_pose = precise_poses[uid][t + 1]
            if prev_pose is None or next_pose is None:
                continue
            angular_velocity = arccos(clip(dot(prev_pose[2:], curr_pose[2:]), -1, 1))
            if angular_velocity > max_angular_velocity:
                precise_poses[uid][t] = None

    # Find out which head poses need to be interpolated
    missing_poses = {
        uid: [t for t in range(num_frames) if precise_poses[uid][t] is None] for uid in identities
    }

    # Interpolate missing poses linearly
    interp_poses = deepcopy(precise_poses)
    for uid in identities:
        tp = [t for t in range(num_frames) if precise_poses[uid][t] is not None]
        fp = list(zip(*[precise_poses[uid][t] for t in tp]))
        for t in missing_poses[uid]:
            interp_poses[uid][t] = float32([interp(t, tp, fp[i]) for i in range(4)])

    trackset.attrs["world"] = world
    trackset.attrs["num_frames"] = num_frames
    trackset.attrs["time_step"] = time_step
    trackset.attrs["tracking_interval"] = tracking_interval
    trackset.attrs["video"] = video_name
    for uid in identities:
        track = trackset.create_dataset(
            f"{uid}",
            shape=(num_frames, 4),
            dtype=float32,
            chunks=True,
            data=float32(interp_poses[uid]),
        )
        track.attrs["interp_frames"] = missing_poses[uid]


def main(args=None):
    def parse_args(args):
        p = ArgumentParser()

        p.add_argument("session_path", type=Path, help="Path to the idtracker.mb_ba session")

        p.add_argument(
            "-o",
            "--output-file",
            type=Path,
            help="Path to a trackset file",
            metavar="trackset",
            dest="trackset",
        )

        p.add_argument(
            "--camera-matrix",
            metavar="ROW",
            help="Row-major entries in camera matrix for the camera that captured the tracked video",
            type=lambda s: [*literal_eval(s)],
            nargs=3,
            default=[
                [2.3368839547577359e03, 0, 1024],
                [0, 2.3368839547577359e03, 1024],
                [0, 0, 1],
            ],
        )

        p.add_argument(
            "--distortion-coefficients",
            help="Distortion coefficients for the camera that captured the tracked video",
            type=lambda s: [*literal_eval(s)],
            default=[
                -1.8315561607912903e-01,
                -1.2699705641214577e-02,
                0,
                0,
                4.4266130527022285e-01,
            ],
        )

        p.add_argument(
            "--corners",
            metavar="CORNER",
            type=lambda s: tuple(*literal_eval(s)),
            nargs=4,
            default=[(153, 241), (181, 1889), (1815, 1860), (1790, 221)],
        )

        p.add_argument(
            "--world",
            help="width,height of the world in centimeters",
            type=lambda s: float32([*literal_eval(s)]),
            default=float32([100, 100]),
        )

        p.add_argument(
            "--max_angular_velocity",
            help="Maximum permissible angular velocity in degrees a tracked fish may exhibit before a frame gets thrown away",
            type=lambda degrees: degrees * pi / 180,
            default=pi / 2,
        )

        return p.parse_args(args)

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    run(
        args.session_path,
        (args.camera_matrix, args.distortion_coefficients),
        args.corners,
        args.world,
        args.max_angular_velocity,
        trackset=args.trackset,
    )
