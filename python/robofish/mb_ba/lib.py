import platform

# Ensure the C++ extension's dependencies are dynamically loaded
if platform.system() != "Linux":
    import PyQt5.QtCore
    import PyQt5.QtGui
    import PyQt5.QtWidgets
    import PyQt5.QtOpenGL
    import mxnet

from robofish.mb_ba.cpp import *
