import sys

assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"
