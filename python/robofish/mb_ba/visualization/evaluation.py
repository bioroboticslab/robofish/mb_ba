from itertools import combinations

import numpy
from numpy import pi

import pandas
from pandas import DataFrame, Series

from matplotlib import pyplot
import seaborn

from robofish.mb_ba.lib import calc_locomotion
from robofish.mb_ba.analysis import calc_iid, calc_follow, calc_tlvc


def plot_trajectories(world, trackset):
    data = {
        uid: DataFrame(
            {
                "x": [pose[0] for pose in trackset[uid]],
                "y": [pose[1] for pose in trackset[uid]],
                "dx": [pose[2] for pose in trackset[uid]],
                "dy": [pose[3] for pose in trackset[uid]],
            }
        )
        for uid in trackset.keys()
    }
    combined_data = pandas.concat([data[uid].assign(Agent=f"Agent {uid}") for uid in data.keys()])

    fig, ax = pyplot.subplots(figsize=(6, 6))

    seaborn.set_style("white", {"axes.linewidth": 2, "axes.edgecolor": "black"})

    seaborn.scatterplot(x="x", y="y", hue="Agent", linewidth=0, s=16, data=combined_data, ax=ax)
    ax.set_xlim(0, world[0])
    ax.set_ylim(0, world[1])
    ax.invert_yaxis()
    ax.xaxis.set_ticks_position("top")
    ax.xaxis.set_label_position("top")
    ax.yaxis.set_ticks_position("left")
    ax.yaxis.set_label_position("left")

    ax.scatter(
        [frame["x"][0] for frame in data.values()],
        [frame["y"][0] for frame in data.values()],
        marker="h",
        c="black",
        s=64,
        label="Start",
    )
    ax.scatter(
        [frame["x"][len(frame["x"]) - 1] for frame in data.values()],
        [frame["y"][len(frame["y"]) - 1] for frame in data.values()],
        marker="x",
        c="black",
        s=64,
        label="End",
    )
    ax.legend()

    return fig


def plot_tankpositions(tracksets):
    x_pos = []
    y_pos = []
    for trackset in tracksets:
        tracks = list(x[:] for x in trackset.values())
        for a in tracks:
            x_pos.append(a[:, 0])
            y_pos.append(a[:, 1])
    x_pos = numpy.concatenate(x_pos, axis=0)
    y_pos = numpy.concatenate(y_pos, axis=0)

    fig, ax = pyplot.subplots(figsize=(6, 6))
    fig.subplots_adjust(top=0.91)
    ax.set_xlim(0, 100)
    ax.set_ylim(0, 100)
    seaborn.kdeplot(x_pos, y_pos, n_levels=25, shade=True, ax=ax)
    return fig


def plot_velocities(tracksets):
    angular_velocities = []
    linear_velocities = []

    for trackset in tracksets:
        for track in trackset.values():
            locomotions = calc_locomotion(track[:-1], track[1:])
            angular_velocities.append(locomotions[:, 0])
            linear_velocities.append(locomotions[:, 1])

    angular_velocities = numpy.concatenate(angular_velocities, axis=0)
    linear_velocities = numpy.concatenate(linear_velocities, axis=0)

    fig_angular, ax = pyplot.subplots(figsize=(6, 6))
    fig_angular.subplots_adjust(top=0.93)
    ax.set_xlim(-pi, pi)
    seaborn.distplot(Series(angular_velocities, name="Angular velocities"), ax=ax)

    fig_linear, ax = pyplot.subplots(figsize=(6, 6))
    fig_linear.subplots_adjust(top=0.93)
    ax.set_xlim(-1.5, 1.5)
    seaborn.distplot(Series(linear_velocities, name="Linear velocities"), ax=ax)

    return fig_angular, fig_linear


def plot_follow_iid(tracksets):
    follow = []
    iid = []

    for trackset in tracksets:
        tracks = list(x[:] for x in trackset.values())
        for a, b in combinations(tracks, 2):
            iid.append(calc_iid(a[:-1], b[:-1]))
            iid.append(iid[-1])

            follow.append(calc_follow(a, b))
            follow.append(calc_follow(b, a))
    follow_iid_data = DataFrame(
        {"IID [cm]": numpy.concatenate(iid, axis=0), "Follow": numpy.concatenate(follow, axis=0)}
    )

    grid = seaborn.jointplot(
        x="IID [cm]", y="Follow", data=follow_iid_data, linewidth=0, s=1, kind="scatter"
    )
    grid.ax_joint.set_xlim(0, 142)
    grid.fig.set_figwidth(9)
    grid.fig.set_figheight(6)
    grid.fig.subplots_adjust(top=0.9)
    return grid.fig


def plot_tlvc_iid(tracksets, time_step, *, tau_seconds=(0.3, 1.3)):
    tau_min_seconds, tau_max_seconds = tau_seconds

    tlvc = []
    iid = []

    tau_min_frames = int(tau_min_seconds * 1000.0 / time_step)
    tau_max_frames = int(tau_max_seconds * 1000.0 / time_step)

    for trackset in tracksets:
        tracks = list(x[:] for x in trackset.values())
        for a, b in combinations(tracks, 2):
            iid.append(calc_iid(a[1 : -tau_max_frames + 1], b[1 : -tau_max_frames + 1]))
            iid.append(iid[-1])

            a_v = a[1:, :2] - a[:-1, :2]
            b_v = b[1:, :2] - b[:-1, :2]
            tlvc.append(calc_tlvc(a_v, b_v, tau_min_frames, tau_max_frames))
            tlvc.append(calc_tlvc(b_v, a_v, tau_min_frames, tau_max_frames))
    tlvc_iid_data = DataFrame(
        {"IID [cm]": numpy.concatenate(iid, axis=0), "TLVC": numpy.concatenate(tlvc, axis=0)}
    )

    grid = seaborn.jointplot(
        x="IID [cm]", y="TLVC", data=tlvc_iid_data, linewidth=0, s=1, kind="scatter"
    )
    grid.ax_joint.set_xlim(0, 142)
    grid.fig.set_figwidth(9)
    grid.fig.set_figheight(6)
    grid.fig.subplots_adjust(top=0.9)
    return grid.fig
