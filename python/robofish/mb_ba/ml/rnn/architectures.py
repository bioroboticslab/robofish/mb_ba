__all__ = ["Eyjolfsdottir"]

from mxnet import ndarray
from mxnet.gluon import HybridBlock


class Eyjolfsdottir(HybridBlock):
    def __init__(self, prefix=None, params=None):
        super().__init__(prefix, params)

        self.num_layers = 0
        self.dis_layers = []
        self.gen_layers = []

    def add(self, *blocks):
        for dis_block, gen_block in blocks:
            self.register_child(dis_block)
            self.dis_layers.append(dis_block)
            self.register_child(gen_block)
            self.gen_layers.append(gen_block)
            self.num_layers += 1

    @property
    def layers(self):
        return self.dis_layers + self.gen_layers

    def hybrid_forward(self, F, x, states):
        dis_states, gen_states = states[: self.num_layers], states[self.num_layers :][::-1]

        bottom_layer = 0
        layer_results = [self.dis_layers[bottom_layer](x, dis_states[bottom_layer])]
        for l in range(1, self.num_layers):
            layer_results.append(self.dis_layers[l](layer_results[-1][0], dis_states[l]))

        dis_out = (layer_results[-1][0] + 1.0) / 2.0

        top_layer = self.num_layers - 1
        layer_results.append(
            self.gen_layers[top_layer](layer_results[-1][0], gen_states[top_layer])
        )
        for l in range(top_layer - 1, -1, -1):
            layer_results.append(
                self.gen_layers[l](
                    F.concat(layer_results[-1][0], layer_results[l][0], dim=1), gen_states[l]
                )
            )

        gen_out = layer_results[-1][0]

        next_states = [results[1] for results in layer_results]
        return (dis_out, gen_out), next_states

    def state_info(self, batch_size=0):
        return [layer.state_info(batch_size=batch_size) for layer in self.layers]

    def begin_state(self, batch_size=0, func=ndarray.zeros, **kwargs):
        return [
            layer.begin_state(batch_size=batch_size, func=func, **kwargs) for layer in self.layers
        ]

    def reset(self):
        for layer in self.layers:
            if callable(getattr(layer, "reset", None)):
                layer.reset()

    def __repr__(self):
        return f"{self.__class__.__name__}({','.join(repr(layer) for layer in self.layers)})"
