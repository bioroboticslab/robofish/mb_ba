__all__ = ["LayerNormLSTMCell", "LayerNormGRUCell"]

from mxnet.gluon.nn import LayerNorm
from mxnet.gluon.rnn import HybridRecurrentCell as _HybridRecurrentCell


class HybridRecurrentCell(_HybridRecurrentCell):
    def __init__(self, prefix=None, params=None):
        super().__init__(prefix=prefix, params=params)

    def reset(self):
        """Reset before re-using the cell for another graph."""
        self._init_counter = -1
        self._counter = -1
        for cell in self._children.values():
            if callable(getattr(cell, "reset", None)):
                cell.reset()


class LayerNormLSTMCell(HybridRecurrentCell):
    def __init__(
        self,
        hidden_size,
        i2h_weight_initializer=None,
        h2h_weight_initializer=None,
        ln_gamma_initializer="ones",
        ln_beta_initializer="zeros",
        input_size=0,
        prefix=None,
        params=None,
    ):
        super().__init__(prefix=prefix, params=params)

        self._hidden_size = hidden_size
        self._input_size = input_size
        self.i2h_weight = self.params.get(
            "i2h_weight",
            shape=(4 * hidden_size, input_size),
            init=i2h_weight_initializer,
            allow_deferred_init=True,
        )
        self.h2h_weight = self.params.get(
            "h2h_weight",
            shape=(4 * hidden_size, hidden_size),
            init=h2h_weight_initializer,
            allow_deferred_init=True,
        )

        with self.name_scope():
            self.norm_in = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_forget = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_transform = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_out = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_c = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )

    def state_info(self, batch_size=0):
        return [
            {"shape": (batch_size, self._hidden_size), "__layout__": "NC"},
            {"shape": (batch_size, self._hidden_size), "__layout__": "NC"},
        ]

    def _alias(self):
        return "lstm"

    def __repr__(self):
        s = "{name}({mapping})"
        shape = self.i2h_weight.shape
        mapping = "{0} -> {1}".format(shape[1] if shape[1] else None, shape[0])
        return s.format(name=self.__class__.__name__, mapping=mapping, **self.__dict__)

    def hybrid_forward(self, F, inputs, states, i2h_weight, h2h_weight):
        prefix = "t%d_" % self._counter
        i2h = F.FullyConnected(
            data=inputs,
            weight=i2h_weight,
            no_bias=True,
            num_hidden=self._hidden_size * 4,
            name=prefix + "i2h",
        )
        h2h = F.FullyConnected(
            data=states[0],
            weight=h2h_weight,
            no_bias=True,
            num_hidden=self._hidden_size * 4,
            name=prefix + "h2h",
        )
        gates = i2h + h2h
        slice_gates = F.SliceChannel(gates, num_outputs=4, name=prefix + "slice")
        in_gate = F.Activation(self.norm_in(slice_gates[0]), act_type="sigmoid", name=prefix + "i")
        forget_gate = F.Activation(
            self.norm_forget(slice_gates[1]), act_type="sigmoid", name=prefix + "f"
        )
        in_transform = F.Activation(
            self.norm_transform(slice_gates[2]), act_type="tanh", name=prefix + "c"
        )
        out_gate = F.Activation(
            self.norm_out(slice_gates[3]), act_type="sigmoid", name=prefix + "o"
        )
        next_c = self.norm_c(
            F._internal._plus(
                forget_gate * states[1], in_gate * in_transform, name=prefix + "state"
            )
        )
        next_h = F._internal._mul(
            out_gate, F.Activation(next_c, act_type="tanh"), name=prefix + "out"
        )

        return next_h, [next_h, next_c]


class LayerNormGRUCell(HybridRecurrentCell):
    def __init__(
        self,
        hidden_size,
        i2h_weight_initializer=None,
        h2h_weight_initializer=None,
        ln_gamma_initializer="ones",
        ln_beta_initializer="zeros",
        input_size=0,
        prefix=None,
        params=None,
    ):
        super().__init__(prefix=prefix, params=params)
        self._hidden_size = hidden_size
        self._input_size = input_size
        self.i2h_weight = self.params.get(
            "i2h_weight",
            shape=(3 * hidden_size, input_size),
            init=i2h_weight_initializer,
            allow_deferred_init=True,
        )
        self.h2h_rz_weight = self.params.get(
            "h2h_rz_weight",
            shape=(2 * hidden_size, hidden_size),
            init=h2h_weight_initializer,
            allow_deferred_init=True,
        )
        self.h2h_h_weight = self.params.get(
            "h2h_h_weight",
            shape=(hidden_size, hidden_size),
            init=h2h_weight_initializer,
            allow_deferred_init=True,
        )

        with self.name_scope():
            self.norm_0 = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_1 = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )
            self.norm_2 = LayerNorm(
                in_channels=hidden_size,
                gamma_initializer=ln_gamma_initializer,
                beta_initializer=ln_beta_initializer,
            )

    def state_info(self, batch_size=0):
        return [{"shape": (batch_size, self._hidden_size), "__layout__": "NC"}]

    def _alias(self):
        return "gru"

    def hybrid_forward(self, F, inputs, states, *, i2h_weight, h2h_rz_weight, h2h_h_weight):
        prefix = f"t{self._counter}_"
        prev_state_h = states[0]

        i2h = F.FullyConnected(
            data=inputs,
            weight=i2h_weight,
            no_bias=True,
            num_hidden=self._hidden_size * 3,
            name=prefix + "i2h",
        )
        h2h_rz = F.FullyConnected(
            data=prev_state_h,
            weight=h2h_rz_weight,
            no_bias=True,
            num_hidden=self._hidden_size * 2,
            name=prefix + "h2h_rz",
        )

        i2h_r, i2h_z, i2h_h = F.SliceChannel(i2h, num_outputs=3, name=prefix + "i2h_slice")
        h2h_r, h2h_z = F.SliceChannel(h2h_rz, num_outputs=2, name=prefix + "h2h_slice")

        reset_gate = F.Activation(
            self.norm_0(i2h_r + h2h_r), act_type="sigmoid", name=prefix + "r_act"
        )
        update_gate = F.Activation(
            self.norm_1(i2h_z + h2h_z), act_type="sigmoid", name=prefix + "z_act"
        )

        h2h_h = F.FullyConnected(
            data=reset_gate * prev_state_h,
            weight=h2h_h_weight,
            no_bias=True,
            num_hidden=self._hidden_size,
            name=prefix + "h2h_h",
        )

        next_h_tmp = F.Activation(
            self.norm_2(i2h_h + h2h_h), act_type="tanh", name=prefix + "h_act"
        )

        next_h = F._internal._plus(
            (1.0 - update_gate) * prev_state_h, update_gate * next_h_tmp, name=prefix + "out"
        )

        return next_h, [next_h]
