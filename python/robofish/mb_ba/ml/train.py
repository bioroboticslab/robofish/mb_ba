import sys
from pathlib import Path
from argparse import ArgumentParser, Action
from math import floor, ceil, log10, copysign, inf
from datetime import datetime
from collections import namedtuple
from signal import signal, SIGINT, SIGTERM
from multiprocessing import cpu_count
from itertools import chain

import h5py

import numpy
from numpy import (
    zeros,
    linspace,
    digitize,
    clip,
    absolute,
    swapaxes,
    concatenate,
    int32,
    float32,
    radians,
    array2string,
)
from numpy.random import RandomState

import mxnet
from mxnet import ndarray, symbol
from mxnet.gluon.nn import Dense, HybridLambda, HybridBlock
from mxnet.gluon.rnn import RecurrentCell
from mxnet.gluon.contrib.rnn import VariationalDropoutCell, Conv1DLSTMCell
from mxnet.gluon import Trainer
from mxnet.gluon.loss import SoftmaxCrossEntropyLoss
from mxnet.context import current_context

from robofish.mb_ba.lib import (
    calc_locomotion,
    ArrayBody,
    wall_distances,
    centroid_distances,
    intensity_linear,
)
from robofish.mb_ba.util import randseed, timestamp
from robofish.mb_ba.ml.rnn.cells import LayerNormLSTMCell, LayerNormGRUCell

from robofish.mb_ba.ml.rnn.basic import Unroller, RecurrentSequential
from robofish.mb_ba.ml.rnn.util import symbol_states
from robofish.mb_ba.ml.rnn.architectures import Eyjolfsdottir
from robofish.mb_ba.ml.nn.layers import Split, Activation


def flatten(S):
    if S == [] or S == ():
        return []
    if isinstance(S[0], list) or isinstance(S[0], tuple):
        return flatten(S[0]) + flatten(S[1:])
    return S[:1] + flatten(S[1:])


def raycast_agent_centroids_and_walls(
    world, trackset, num_frames, view_of_agents, view_of_walls, far_plane, out, offset=0
):
    world = float32(world)
    num_frames = int(num_frames)

    view_of_agents_size, view_of_agents_sectors = view_of_agents
    view_of_walls_size, view_of_walls_rays = view_of_walls

    uids = tuple(trackset.keys())

    for t in range(num_frames):
        bodies = {
            uid: ArrayBody(int(uid), trackset[uid][t][0:2], trackset[uid][t][2:4]) for uid in uids
        }
        for uid in uids:
            if view_of_agents_size:
                view = out[uid][t][offset:][:view_of_agents_size]
                view[:] = centroid_distances(
                    bodies[uid], list(bodies.values()), view_of_agents_sectors
                )
                view[:] = intensity_linear(view, far_plane)

            if view_of_walls_size:
                view = out[uid][t][offset + view_of_agents_size :][:view_of_walls_size]
                view[:] = wall_distances(bodies[uid], world, view_of_walls_rays)
                view[:] = intensity_linear(view, far_plane)


LOCOMOTION_SIZE = 2


def extract_features(world, trackset, num_frames, view_of_agents, view_of_walls, far_plane):
    num_frames = int(num_frames)

    view_of_agents_size, _ = view_of_agents
    view_of_walls_size, _ = view_of_walls

    uids = tuple(trackset.keys())
    features = {
        uid: zeros(
            (num_frames, LOCOMOTION_SIZE + view_of_agents_size + view_of_walls_size), dtype=float32
        )
        for uid in uids
    }

    # Step 1: Locomotion from previous frame, first locomotion is "at rest" / zero vector
    for uid in uids:
        features[uid][1:, :LOCOMOTION_SIZE] = calc_locomotion(
            trackset[uid][:-1], trackset[uid][1:]
        )

    # Step 2: Sensory input
    raycast_agent_centroids_and_walls(
        world,
        trackset,
        num_frames,
        view_of_agents,
        view_of_walls,
        far_plane,
        features,
        LOCOMOTION_SIZE,
    )

    return {uid: features[uid] for uid in uids}


def label_locomotions(features, locomotion, *, epsilon=1e-7):

    num_frames = len(features) - 1

    labels = zeros((num_frames, LOCOMOTION_SIZE), dtype=int32)

    locomotion_bins = [linspace(*component[:2], component[2] + 1) for component in locomotion]
    for t in range(num_frames):
        labels[t][:] = [
            digitize(clip(value, bins[0], bins[-1] - epsilon), bins) - 1
            for value, bins in zip(features[t + 1][:LOCOMOTION_SIZE], locomotion_bins)
        ]

    return labels


def prepare_view_bins(view_of_agents, view_of_walls):
    if view_of_agents:
        fop, view_of_agents_size = view_of_agents
        view_of_agents_sectors = linspace(
            -radians(fop) / 2.0, radians(fop) / 2.0, view_of_agents_size + 1, dtype=float32
        )
    else:
        view_of_agents_size = 0
        view_of_agents_sectors = []

    if view_of_walls:
        fop, view_of_walls_size = view_of_walls
        view_of_walls_rays = linspace(
            -radians(fop) / 2.0, radians(fop) / 2.0, view_of_walls_size, dtype=float32
        )
    else:
        view_of_walls_size = 0
        view_of_walls_rays = []

    return (view_of_agents_size, view_of_agents_sectors), (view_of_walls_size, view_of_walls_rays)


def load_trackset(trackset):
    with h5py.File(trackset, "r") as f:
        return (
            f.attrs["world"],
            {key: f[key][:] for key in f.keys() if isinstance(f[key], h5py.Dataset)},
            f.attrs["num_frames"],
        )


def process_tracksets(
    training_tracksets,
    validation_tracksets,
    view_of_agents,
    view_of_walls,
    *,
    far_plane,
    locomotion,
):
    assert training_tracksets, "At least one trackset is required for training"
    assert validation_tracksets, "At least one trackset is required for validation"

    def extract(trackset):
        return (
            trackset,
            extract_features(*load_trackset(trackset), view_of_agents, view_of_walls, far_plane),
        )

    features_tr = dict(extract(trackset) for trackset in training_tracksets)
    features_va = dict(extract(trackset) for trackset in validation_tracksets)
    features = [b for a in features_tr.values() for b in a.values()] + [
        b for a in features_va.values() for b in a.values()
    ]

    unique_timesteps = set(
        h5py.File(trackset, "r").attrs["time_step"]
        for trackset in chain(training_tracksets, validation_tracksets)
    )
    assert len(unique_timesteps) == 1, "Only tracksets with time-equidistant frames are supported"
    time_step = unique_timesteps.pop()

    def finalize_track(features, locomotion):
        labels = label_locomotions(features, locomotion)
        features = numpy.delete(features, -1, axis=0)  # No labels for last frame
        return features, labels

    def finalize_trackset(features_per_track, locomotion):
        def task(uid, features):
            return uid, finalize_track(features, locomotion)

        return dict(task(uid, features) for uid, features in features_per_track.items())

    def finalize(trackset, features_per_track):
        return trackset, finalize_trackset(features_per_track, locomotion)

    examples_tr = dict(
        finalize(trackset, features_per_track)
        for trackset, features_per_track in features_tr.items()
    )
    examples_va = dict(
        finalize(trackset, features_per_track)
        for trackset, features_per_track in features_va.items()
    )

    return examples_tr, examples_va, time_step


class ExampleDataset:
    def __init__(self, examples, batch_size, ctx=current_context(), random=None, *, shuffle=False):
        self.examples = examples
        self.batch_size = batch_size
        self.ctx = ctx
        self.random = random or RandomState()
        self.shuffle = shuffle

        self.num_examples = len(self.examples)
        assert 0 < self.batch_size <= self.num_examples

        self.num_sequences = None
        for data_sequences, label_sequences in self.examples:
            if self.num_sequences is not None:
                assert self.num_sequences == len(data_sequences)
                assert self.num_sequences == len(label_sequences)
            else:
                self.num_sequences = len(data_sequences)
                assert self.num_sequences == len(label_sequences)

    def _batchify(self):
        if self.shuffle:
            self.random.shuffle(self.examples)
        data, labels = zip(*self.examples)
        # Change layout: ESTC -> SETC (E: number of examples, S: number of sequences)
        data = swapaxes(data, 0, 1)
        labels = swapaxes(labels, 0, 1)
        # Change layout: SETC -> STEC
        data = swapaxes(data, 1, 2)
        labels = swapaxes(labels, 1, 2)

        # Change layout: STEC -> SBTNC (B: number of batches, N: batch size)
        data_batched_sequences = []
        label_batched_sequences = []
        for sequence in range(self.num_sequences):
            data_batches = []
            label_batches = []
            for index in range(0, self.num_examples, self.batch_size):
                data_batches.append(data[sequence][:, index : index + self.batch_size])
                label_batches.append(labels[sequence][:, index : index + self.batch_size])

            # Fixup last batch if necessary
            remainder = self.num_examples % self.batch_size
            if remainder:
                missing = self.batch_size - remainder
                # Roll over to beginning and reuse examples to complete last batch
                data_batches[-1] = concatenate(
                    (data_batches[-1], data[sequence][:, :missing]), axis=1
                )
                label_batches[-1] = concatenate(
                    (label_batches[-1], labels[sequence][:, :missing]), axis=1
                )
            data_batched_sequences.append(data_batches)
            label_batched_sequences.append(label_batches)
        # Change layout: SBTNC -> BSTNC
        data_batched_sequences = swapaxes(data_batched_sequences, 0, 1)
        label_batched_sequences = swapaxes(label_batched_sequences, 0, 1)

        data_batched_sequences = [
            [ndarray.array(batch, dtype=batch.dtype, ctx=self.ctx) for batch in batches]
            for batches in data_batched_sequences
        ]
        label_batched_sequences = [
            [ndarray.array(batch, dtype=batch.dtype, ctx=self.ctx) for batch in batches]
            for batches in label_batched_sequences
        ]

        return data_batched_sequences, label_batched_sequences

    def __iter__(self):
        def iterate():
            for data, labels in zip(*self._batchify()):
                yield data, labels

        return iterate()


def sequence_examples(examples_per_trackset, sequence_length):
    result = {}
    for trackset, examples_per_track in examples_per_trackset.items():
        sequenced_examples = []
        num_sequences = None
        for uid, (features, labels) in examples_per_track.items():
            assert sequence_length <= len(
                features
            ), "Sequence length must not exceed available data"
            length = min(len(features), len(labels))
            stop = (length // sequence_length) * sequence_length
            feature_sequences = tuple(
                features[offset:][:sequence_length] for offset in range(0, stop, sequence_length)
            )
            label_sequences = tuple(
                labels[offset:][:sequence_length] for offset in range(0, stop, sequence_length)
            )
            assert len(feature_sequences) == len(label_sequences)
            if num_sequences is not None:
                assert num_sequences == len(feature_sequences)
            else:
                num_sequences = len(feature_sequences)
            sequenced_examples.append((uid, feature_sequences, label_sequences))
        if not num_sequences in result:
            result[num_sequences] = {trackset: sequenced_examples}
        else:
            result[num_sequences][trackset] = sequenced_examples

    return result


class ViewNetwork(RecurrentSequential):
    LOCOMOTION_SIZE = LOCOMOTION_SIZE
    NUM_VIEW_VECTORS = 2

    def __init__(self, view_size, prefix=None, params=None):
        super().__init__(prefix=prefix, params=params)
        self.view_size = view_size

    def hybrid_forward(self, F, x, states):
        locomotion = x.slice_axis(-1, 0, self.LOCOMOTION_SIZE)
        view_agents = x.slice_axis(-1, self.LOCOMOTION_SIZE, self.LOCOMOTION_SIZE + self.view_size)
        view_walls = x.slice_axis(-1, self.LOCOMOTION_SIZE + self.view_size, None)

        x = F.stack(view_agents, view_walls, axis=1)

        next_states = []
        for layer, layer_states in zip(self.layers, states):
            if layer_states:
                x, layer_next_states = layer(x, layer_states)
            else:
                x, layer_next_states = layer(x), ()
            next_states.append(layer_next_states)

        x = F.concat(locomotion, F.flatten(x))
        return x, next_states


def training_model(
    view_convolutions,
    view_size,
    action_arch,
    action_num_layers,
    action_cell,
    action_size,
    variational_dropout=None,
):
    stack = RecurrentSequential()
    with stack.name_scope():
        if view_convolutions:
            view_stack = ViewNetwork(view_size)
            stack.add(view_stack)
            with view_stack.name_scope():
                input_shape = view_stack.NUM_VIEW_VECTORS, view_size
                for hidden_channels, i2h_kernel, h2h_kernel in view_convolutions:
                    i2h_dilate = 2
                    h2h_dilate = 2
                    i2h_pad = int(floor((i2h_kernel + (i2h_kernel - 1) * (i2h_dilate - 1)) / 2))
                    view_layer = Conv1DLSTMCell(
                        input_shape,
                        hidden_channels,
                        i2h_kernel,
                        h2h_kernel,
                        i2h_pad,
                        i2h_dilate,
                        h2h_dilate,
                    )
                    view_stack.add(view_layer)
                    input_shape = hidden_channels, view_size

        action_cell = cell_type(action_cell)
        if action_arch == "srn":
            for _ in range(action_num_layers):
                action_layer = action_cell(action_size)
                if variational_dropout is not None:
                    action_layer = VariationalDropoutCell(action_layer, *variational_dropout)
                stack.add(action_layer)
        elif action_arch == "eyj":
            action_stack = Eyjolfsdottir()
            stack.add(action_stack)
            with action_stack.name_scope():
                for _ in range(action_num_layers):
                    action_layers = action_cell(action_size), action_cell(action_size)
                    if variational_dropout is not None:
                        action_layers = tuple(
                            VariationalDropoutCell(l, *variational_dropout) for l in action_layers
                        )
                    action_stack.add(action_layers)
            stack.add(HybridLambda(lambda F, dis, gen: gen))
        else:
            assert False

    return stack


def binned_training_model(
    view_convolutions,
    view_size,
    action_arch,
    action_num_layers,
    action_cell,
    action_size,
    locomotion_num_bins,
    variational_dropout=None,
):
    stack = training_model(
        view_convolutions,
        view_size,
        action_arch,
        action_num_layers,
        action_cell,
        action_size,
        variational_dropout=variational_dropout,
    )
    with stack.name_scope():
        stack.add(Dense(sum(locomotion_num_bins)))
        stack.add(Split(locomotion_num_bins))
    return stack


def binned_inference_model(
    view_convolutions,
    view_size,
    action_arch,
    action_num_layers,
    action_cell,
    action_size,
    locomotion_num_bins,
):
    stack = training_model(
        view_convolutions,
        view_size,
        action_arch,
        action_num_layers,
        action_cell,
        action_size,
        variational_dropout=None,
    )
    with stack.name_scope():
        stack.add(Dense(sum(locomotion_num_bins)))
        stack.add(Split(locomotion_num_bins))
        stack.add(Activation("softmax"))
    return stack


def cell_type(name):
    return {"lstm": LayerNormLSTMCell, "gru": LayerNormGRUCell}[name]


def train(
    arch,
    num_layers,
    cell,
    num_hidden,
    training_path,
    validation_path,
    *,
    output_directory=Path("."),
    output_file=None,
    tags=[],
    seed=None,
    view_of_agents=[],
    view_of_walls=[],
    far_plane=142.0,
    locomotion,
    num_epochs=1,
    target_accuracy=0.9,
    gpu=False,
    batch_size=1,
    sequence_length=inf,
    drop_inputs=0.0,
    drop_states=0.1,
    view_convolutions,
):
    print("=== Model ===", flush=True)
    if view_convolutions:
        for cnv_channels, cnv_i2h_kernel, cnv_h2h_kernel in view_convolutions:
            print(
                f"View Layer: ConvLSTM, Hidden channels: {cnv_channels}, I2H kernel: {cnv_i2h_kernel}, H2H kernel: {cnv_h2h_kernel}",
                flush=True,
            )
    print(
        f"Action Architecture: {arch}, Layers: {num_layers}, Cell: {cell}, Hidden units: {num_hidden}",
        flush=True,
    )

    seed = seed if seed is not None else randseed()
    random = RandomState(seed)
    mxnet.random.seed(randseed(random))

    if view_of_walls == "match" and view_of_agents:
        fop, n = view_of_agents
        view_of_walls = fop - fop / n, n
    (view_of_agents_size, view_of_agents_sectors), (
        view_of_walls_size,
        view_of_walls_rays,
    ) = prepare_view_bins(view_of_agents, view_of_walls)

    tracksets_tr = list(Path(training_path).glob("*.hdf5"))
    tracksets_va = list(Path(validation_path).glob("*.hdf5"))

    if view_of_agents:
        print(f"View of Agents: {view_of_agents}", flush=True)
    if view_of_walls:
        print(f"View of Walls:  {view_of_walls}", flush=True)
    examples_tr, examples_va, time_step = process_tracksets(
        tracksets_tr,
        tracksets_va,
        (view_of_agents_size, view_of_agents_sectors),
        (view_of_walls_size, view_of_walls_rays),
        far_plane=far_plane,
        locomotion=locomotion,
    )
    print(
        f"Angular locomotion: (-{locomotion[0][1]:.02f}, {locomotion[0][1]:.02f}, {locomotion[0][2]})",
        flush=True,
    )
    print(
        f"Linear  locomotion: (-{locomotion[1][1]:.02f}, {locomotion[1][1]:.02f}, {locomotion[1][2]})",
        flush=True,
    )

    if sequence_length == inf:
        for examples in chain(examples_tr.values(), examples_va.values()):
            for features, labels in examples.values():
                assert len(features) == len(labels)
                if sequence_length != inf:
                    assert sequence_length == len(
                        features
                    ), "Only uniformly sized examples supported for infinite sequence length"
                else:
                    sequence_length = len(features)
    else:
        for examples in chain(examples_tr.values(), examples_va.values()):
            for features, labels in examples.values():
                assert len(features) == len(labels)
                assert sequence_length <= len(
                    features
                ), "Sequence length must not exceed available data"

    ctx = mxnet.gpu() if gpu else mxnet.cpu()

    sq_examples_tr = sequence_examples(examples_tr, sequence_length)
    sq_examples_va = sequence_examples(examples_va, sequence_length)

    data_tr = [
        ExampleDataset(
            [(a, b) for x in sq_examples.values() for _, a, b in x],
            batch_size,
            ctx,
            random,
            shuffle=True,
        )
        for sq_examples in sq_examples_tr.values()
    ]
    data_va = [
        ExampleDataset(
            [(a, b) for x in sq_examples.values() for _, a, b in x],
            batch_size,
            ctx,
            random,
            shuffle=False,
        )
        for sq_examples in sq_examples_va.values()
    ]

    locomotion_num_bins = locomotion[0][2], locomotion[1][2]

    if view_convolutions:
        assert view_of_agents_size == view_of_walls_size
        view_size = view_of_agents_size
    else:
        view_size = None

    unroller = Unroller(sequence_length)
    with unroller.name_scope():
        model = binned_training_model(
            view_convolutions,
            view_size,
            arch,
            num_layers,
            cell,
            num_hidden,
            locomotion_num_bins,
            variational_dropout=(drop_inputs, drop_states, 0),
        )
        begin_state = model.begin_state(batch_size, func=ndarray.zeros, ctx=ctx)
        unroller.add(model)

    model = unroller
    model.initialize(mxnet.init.Xavier(), ctx)
    model.hybridize()

    loss = SoftmaxCrossEntropyLoss(
        from_logits=False, batch_axis=1  # Unnormalized numbers input  # TNC layout
    )
    loss.hybridize()

    optimizer_params = {"learning_rate": 1e-3, "beta1": 0.9, "beta2": 0.999, "epsilon": 1e-08}
    trainer = Trainer(model.collect_params(), "adam", optimizer_params)

    if output_file:
        training_filename = output_file
    else:
        training_filename = (output_directory / "_".join(tags + [timestamp()])).with_suffix(
            ".hdf5"
        )
    training = h5py.File(training_filename, "w")

    if tags:
        training.attrs["tags"] = tags
    training.attrs["seed"] = seed
    start_time = datetime.utcnow()

    training.attrs["training_tracksets"] = [str(path) for path in tracksets_tr]
    training.attrs["validation_tracksets"] = [str(path) for path in tracksets_va]
    training.attrs["time_step"] = time_step

    training.attrs["arch"] = arch
    training.attrs["num_layers"] = num_layers
    training.attrs["cell"] = cell
    training.attrs["num_hidden"] = num_hidden
    if view_convolutions:
        training.attrs["view_convolutions"] = view_convolutions

    training.attrs["view_of_agents_size"] = view_of_agents_size
    if view_of_agents:
        training.attrs["view_of_agents"] = view_of_agents
        training.attrs["view_of_agents_sectors"] = view_of_agents_sectors
    training.attrs["view_of_walls_size"] = view_of_walls_size
    if view_of_walls:
        training.attrs["view_of_walls"] = view_of_walls
        training.attrs["view_of_walls_rays"] = view_of_walls_rays
    training.attrs["far_plane"] = far_plane

    training.attrs["locomotion"] = locomotion

    training.attrs["target_accuracy"] = target_accuracy
    training.attrs["gpu"] = gpu
    training.attrs["batch_size"] = batch_size
    training.attrs["sequence_length"] = sequence_length
    training.attrs["drop_inputs"] = drop_inputs
    training.attrs["drop_states"] = drop_states

    interrupted = False

    def signal_handler(signum, _):
        nonlocal interrupted
        if signum == SIGINT:
            if not interrupted:
                interrupted = True
            else:
                print("SIGINT received twice", flush=True)
                raise SystemExit()
        elif signum == SIGTERM:
            print("SIGTERM received", flush=True)
            raise SystemExit()

    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)

    def train_batch(batch):
        losses = []
        accuracies = []
        confidences = []
        state = begin_state
        batch_data, batch_labels = batch
        for sequence_data, sequence_labels in zip(batch_data, batch_labels):
            with mxnet.autograd.record():
                (angular_predictions, linear_predictions), state = model(sequence_data, state)
                angular_labels, linear_labels = (
                    sequence_labels.slice_axis(-1, 0, 1),
                    sequence_labels.slice_axis(-1, 1, 2),
                )

                sequence_losses = ndarray.stack(
                    loss(angular_predictions, angular_labels),
                    loss(linear_predictions, linear_labels),
                    axis=-1,
                )

                sequence_losses.backward()

            trainer.step(batch_size)

            sequence_accuracies = ndarray.stack(
                (
                    angular_predictions.argmax(axis=-1).astype(int32)
                    == angular_labels.reshape(angular_predictions.shape[:-1])
                )
                .astype(float32)
                .mean(axis=-1),
                (
                    linear_predictions.argmax(axis=-1).astype(int32)
                    == linear_labels.reshape(angular_predictions.shape[:-1])
                )
                .astype(float32)
                .mean(axis=-1),
                axis=-1,
            )
            sequence_confidences = ndarray.stack(
                angular_predictions.softmax().topk(ret_typ="value").reshape((0, 0)).mean(axis=0),
                linear_predictions.softmax().topk(ret_typ="value").reshape((0, 0)).mean(axis=0),
                axis=-1,
            )

            losses.append(sequence_losses.mean(axis=0))
            accuracies.append(sequence_accuracies.mean(axis=0))
            confidences.append(sequence_confidences.mean(axis=0))

        return (
            ndarray.stack(*losses, axis=0).mean(axis=0),
            ndarray.stack(*accuracies, axis=0).mean(axis=0),
            ndarray.stack(*confidences, axis=0).mean(axis=0),
        )

    def eval_batch(batch):
        losses = []
        accuracies = []
        confidences = []
        state = begin_state
        batch_data, batch_labels = batch
        for sequence_data, sequence_labels in zip(batch_data, batch_labels):
            (angular_predictions, linear_predictions), state = model(sequence_data, state)
            angular_labels, linear_labels = (
                sequence_labels.slice_axis(-1, 0, 1),
                sequence_labels.slice_axis(-1, 1, 2),
            )

            sequence_losses = ndarray.stack(
                loss(angular_predictions, angular_labels),
                loss(linear_predictions, linear_labels),
                axis=-1,
            )
            sequence_accuracies = ndarray.stack(
                (
                    angular_predictions.argmax(axis=-1).astype(int32)
                    == angular_labels.reshape(angular_predictions.shape[:-1])
                )
                .astype(float32)
                .mean(axis=-1),
                (
                    linear_predictions.argmax(axis=-1).astype(int32)
                    == linear_labels.reshape(angular_predictions.shape[:-1])
                )
                .astype(float32)
                .mean(axis=-1),
                axis=-1,
            )
            sequence_confidences = ndarray.stack(
                angular_predictions.softmax().topk(ret_typ="value").reshape((0, 0)).mean(axis=0),
                linear_predictions.softmax().topk(ret_typ="value").reshape((0, 0)).mean(axis=0),
                axis=-1,
            )

            losses.append(sequence_losses.mean(axis=0))
            accuracies.append(sequence_accuracies.mean(axis=0))
            confidences.append(sequence_confidences.mean(axis=0))

        return (
            ndarray.stack(*losses, axis=0).mean(axis=0),
            ndarray.stack(*accuracies, axis=0).mean(axis=0),
            ndarray.stack(*confidences, axis=0).mean(axis=0),
        )

    def do_epoch(datasets, do_batch):
        losses = []
        accuracies = []
        confidences = []
        for dataset in datasets:
            for batch in dataset:
                batch_losses, batch_accuracies, batch_confidences = do_batch(batch)
                losses.append(batch_losses)
                accuracies.append(batch_accuracies)
                confidences.append(batch_confidences)
        losses = ndarray.stack(*losses, axis=-1)
        accuracies = ndarray.stack(*accuracies, axis=-1)
        confidences = ndarray.stack(*confidences, axis=-1)
        return losses.mean(-1), accuracies.mean(-1), confidences.mean(-1)

    def collect_state_symbols(block):
        if isinstance(block, RecurrentCell):
            return [
                symbol.var(f"{block.prefix}state{i}") for i, state in enumerate(block.state_info())
            ]
        else:
            return [collect_state_symbols(child) for child in block._children.values()]

    export_model = binned_inference_model(
        view_convolutions, view_size, arch, num_layers, cell, num_hidden, locomotion_num_bins
    )
    outputs, next_state = export_model(symbol.var("feature"), collect_state_symbols(export_model))
    export_model = symbol.Group(list(outputs) + flatten(next_state))
    training.attrs["symbols"] = export_model.tojson()

    try:
        epoch = 0
        params = training.create_group("params")
        format_array = lambda array: array2string(
            array, formatter={"float_kind": lambda x: f"{x:.3f}"}
        )

        print("=== Training ===")
        print(f"Maximum epochs: {num_epochs}, Target accuracy: {target_accuracy}", flush=True)
        while epoch < num_epochs and not interrupted:
            loss_tr, acc_tr, conf_tr = do_epoch(data_tr, train_batch)

            epoch_params = params.create_group(f"{epoch:04}")

            epoch_params.attrs["training_losses"] = loss_tr.asnumpy()
            epoch_params.attrs["training_accuracies"] = acc_tr.asnumpy()
            epoch_params.attrs["training_confidences"] = conf_tr.asnumpy()

            model.save_params_hdf5(epoch_params)

            loss_va, acc_va, conf_va = do_epoch(data_va, eval_batch)

            epoch_params.attrs["validation_losses"] = loss_va.asnumpy()
            epoch_params.attrs["validation_accuracies"] = acc_va.asnumpy()
            epoch_params.attrs["validation_confidences"] = conf_va.asnumpy()
            print(f"=== Epoch {epoch:04} ===")
            print(f"Training   loss:       {format_array(loss_tr.mean(-1).asscalar())}")
            print(f"Validation loss:       {format_array(loss_va.mean(-1).asscalar())}")
            print(f"Validation accuracy:   {format_array(acc_va.mean(-1).asscalar())}")
            print(
                f"Validation confidence: {format_array(conf_va.mean(-1).asscalar())}", flush=True
            )

            try:
                if acc_va.mean(-1).asscalar() >= target_accuracy:
                    print("\nTarget validation accuracy reached", flush=True)
                    raise SystemExit()
            finally:
                epoch += 1
                training.attrs["num_epochs"] = epoch
                end_time = datetime.utcnow()
                training.attrs["time"] = str(end_time - start_time)
                training.flush()
        print("\nMaximum number of epochs reached", flush=True)
    except SystemExit:
        pass
    finally:
        training.close()
        if epoch < 1:
            training_filename.unlink()


def main(args=None):
    def parse_args(args):
        class LocomotionAction(Action):
            def __init__(self, option_strings, dest, nargs=None, **kwargs):
                assert nargs is None
                super().__init__(
                    option_strings,
                    dest,
                    6,
                    metavar=(
                        "ANGULAR_MIN",
                        "ANGULAR_MAX",
                        "ANGULAR_BINS",
                        "LINEAR_MIN",
                        "LINEAR_MAX",
                        "LINEAR_BINS",
                    ),
                    **kwargs,
                )

            def __call__(self, parser, namespace, values, option_string=None):
                setattr(
                    namespace,
                    self.dest,
                    ([int(x) for x in values[0:3]], [int(x) for x in values[3:6]]),
                )

        class ViewAction(Action):
            def __init__(self, option_strings, dest, nargs=None, **kwargs):
                assert nargs is None
                super().__init__(
                    option_strings,
                    dest,
                    2,
                    metavar=("field of perception", "number of bins"),
                    **kwargs,
                )

            def __call__(self, parser, namespace, values, option_string=None):
                items = (float32(values[0]), int(values[1]))
                setattr(namespace, self.dest, items)

        class ViewConvolutionAction(Action):
            def __init__(self, option_strings, dest, nargs=None, **kwargs):
                assert nargs is None
                super().__init__(
                    option_strings,
                    dest,
                    3,
                    metavar=("HIDDEN_CHANNELS", "I2H_KERNEL", "H2H_KERNEL"),
                    default=[],
                    **kwargs,
                )

            def __call__(self, parser, namespace, values, option_string=None):
                items = getattr(namespace, self.dest)
                items.append((int(values[0]), int(values[1]), int(values[2])))
                setattr(namespace, self.dest, items)

        p = ArgumentParser()

        p.add_argument("--tags", default=[], nargs="+", help="Tags attached to the training file")

        o = p.add_mutually_exclusive_group()
        o.add_argument(
            "--output-directory",
            "--od",
            default=Path("."),
            type=Path,
            help="Directory in which to store an automatically named training file",
        )
        o.add_argument(
            "-o", "--output-file", type=Path, help="Path to a manually named training file"
        )

        p.add_argument("--seed", default=None, type=int, help="Root seed for PRNG")

        h = p.add_argument_group("hyper")
        h.add_argument("arch", choices=["srn", "eyj"], help="Neural network architecture to use")
        h.add_argument("num_layers", default=1, type=int, help="Number of layers in the network")
        h.add_argument(
            "cell", choices=["lstm", "gru"], help="Recurrent cell to use in the network"
        )
        h.add_argument(
            "num_hidden", type=int, help="Number of hidden units used in one recurrent cell"
        )
        h.add_argument(
            "--locomotion", action=LocomotionAction, help="Binned locomotion", required=True
        )

        h.add_argument("--view-of-agents", action=ViewAction)
        h.add_argument("--view-of-walls", action=ViewAction)
        h.add_argument(
            "--view-of-walls-matches", action="store_const", dest="view_of_walls", const="match"
        )
        h.add_argument(
            "--far-plane", default=142.0, type=float, help="Maximum distance an agent can see"
        )
        h.add_argument(
            "--view-convolution", action=ViewConvolutionAction, dest="view_convolutions"
        )

        t = p.add_argument_group("training")
        t.add_argument(
            "training_path",
            type=Path,
            metavar="training_path",
            help="Path to directory containing the training tracksets",
        )
        t.add_argument(
            "validation_path",
            type=Path,
            metavar="validation_path",
            help="Path to directory containing the validation tracksets",
        )
        t.add_argument(
            "--num-epochs",
            default=1,
            type=int,
            help="Training lasts for at most this many iterations",
        )
        t.add_argument(
            "--target-accuracy",
            default=0.9,
            type=float,
            help="Training should last at least until the network has achieved this accuracy on the validation examples",
        )
        t.add_argument(
            "-s",
            "--sequence-length",
            default=inf,
            type=int,
            help="Length training examples are sliced to",
        )

        t.add_argument(
            "--gpu",
            default=False,
            action="store_const",
            const=True,
            help="Execute training on the first available GPU instead of the CPU",
        )
        t.add_argument(
            "--batch-size",
            default=1,
            type=int,
            help="Send this many examples through the network at once (as a batch)",
        )
        t.add_argument(
            "--drop-inputs",
            default=0.0,
            type=float,
            help="Fraction of a recurrent cell's input vector to zero out",
        )
        t.add_argument(
            "--drop-states",
            default=0.1,
            type=float,
            help="Fraction of a recurrent cell's state vector to zero out",
        )

        args = p.parse_args(args)

        return args

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    train(**args.__dict__)
