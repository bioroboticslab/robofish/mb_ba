#include <cstdint>
#include <memory>

#include <robofish/mb_ba/RandomState.h++>

#include "python.h++"

namespace robofish::mb_ba::PyRandomState
{
	using namespace pybind11;
	using namespace pybind11::literals;

	static auto uniform_float(RandomState& self, float low, float high)
	{
		return self.uniform(low, high);
	}

	static auto uniform_float_samples(RandomState& self, float low, float high, size_t size)
	{
		return Eigen::VectorXf::NullaryExpr(size, [&]() { return self.uniform(low, high); });
	}

	static auto uniform_int(RandomState& self, int low, int high)
	{
		return self.uniform(low, high);
	}

	static auto uniform_int_samples(RandomState& self, int low, int high, size_t size)
	{
		return Eigen::VectorXf::NullaryExpr(size, [&]() { return self.uniform(low, high); });
	}

	static auto normal(RandomState& self, float location, float scale)
	{
		return self.normal(location, scale);
	}

	static auto normal_samples(RandomState& self, float location, float scale, size_t size)
	{
		return Eigen::VectorXf::NullaryExpr(size, [&]() { return self.normal(location, scale); });
	}

	static auto multivariate_normal(
	   RandomState&                                                                            self,
	   Eigen::Ref<const Eigen::VectorXf>                                                       location,
	   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance)
	{
		return self.multivariate_normal(location, covariance);
	}

	static auto multivariate_normal_samples(
	   RandomState&                                                                            self,
	   Eigen::Ref<const Eigen::VectorXf>                                                       location,
	   Eigen::Ref<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> covariance,
	   size_t                                                                                  size)
	{
		return self.multivariate_normal(location, covariance, size);
	}

	static auto multivariate_normal_mixture(RandomState&                                            self,
	                                        stx::span<float const>                                  weights,
	                                        array_t<const float, array::c_style | array::forcecast> locations,
	                                        array_t<const float, array::c_style | array::forcecast> covariances)
	{
		if (locations.ndim() != 2 || covariances.ndim() != 3 || weights.size() != locations.shape(0)
		    || locations.shape(0) != covariances.shape(0) || locations.shape(1) != covariances.shape(1)
		    || locations.shape(1) != covariances.shape(2)) {
			throw std::invalid_argument("Invalid shape(s)");
		}

		auto selected_mixture_component = self.multinomial(weights);
		auto selected_location          = Eigen::Map<const Eigen::VectorXf>(
         locations.mutable_unchecked().mutable_data(selected_mixture_component, 0),
         locations.shape(1));
		auto selected_covariance
		   = Eigen::Map<const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(
		      covariances.mutable_unchecked().mutable_data(selected_mixture_component, 0, 0),
		      covariances.shape(1),
		      covariances.shape(2));

		return self.multivariate_normal(selected_location, selected_covariance);
	}

	static auto multivariate_normal_mixture_samples(RandomState&                                            self,
	                                                stx::span<float const>                                  weights,
	                                                array_t<const float, array::c_style | array::forcecast> locations,
	                                                array_t<const float, array::c_style | array::forcecast> covariances,
	                                                size_t                                                  size)
	{
		if (locations.ndim() != 2)
			throw std::invalid_argument("Invalid shape(s)");
		auto samples = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>(size, locations.shape(1));
		for (size_t i = 0; i < size; ++i) {
			samples.row(i) = multivariate_normal_mixture(self, weights, locations, covariances);
		}
		return samples;
	}

	void bind(module& m)
	{
		class_<RandomState, std::shared_ptr<RandomState>>(m, "RandomState")
		   .def(init<>())
		   .def(init<RandomState&>(), "seed"_a)
		   .def(init<std::uint32_t>(), "seed"_a)
		   .def("seed", &RandomState::seed, "seed"_a)
		   .def("uniform", &uniform_float, "low"_a, "high"_a)
		   .def("uniform", &uniform_float_samples, "low"_a, "high"_a, "size"_a)
		   .def("uniform", &uniform_int, "low"_a, "high"_a)
		   .def("uniform", &uniform_int_samples, "low"_a, "high"_a, "size"_a)
		   .def("normal", &normal, "location"_a, "scale"_a)
		   .def("normal", &normal_samples, "location"_a, "scale"_a, "size"_a)
		   .def("multivariate_normal", &multivariate_normal, "location"_a, "covariance"_a)
		   .def("multivariate_normal", &multivariate_normal_samples, "location"_a, "covariance"_a, "size"_a)
		   .def("multivariate_normal_mixture", &multivariate_normal_mixture, "weights"_a, "locations"_a, "covariances"_a)
		   .def("multivariate_normal_mixture",
		        &multivariate_normal_mixture_samples,
		        "weights"_a,
		        "locations"_a,
		        "covariances"_a,
		        "size"_a);
	}
}
