#include "python.h++"

#include <robofish/mb_ba/agents/CouzinAgent.h++>
#include <robofish/mb_ba/agents/MXNetAgent.h++>
#include <robofish/mb_ba/common.h++>
#include <robofish/mb_ba/ArrayBody.h++>
#include <robofish/mb_ba/Agent.h++>
#include <robofish/mb_ba/span.h++>

namespace robofish::mb_ba::PyRandomState
{
	void bind(pybind11::module& m);
}
namespace robofish::mb_ba::PyIBody
{
	void bind(pybind11::module& m);
}
namespace robofish::mb_ba::PyArrayBody
{
	void bind(pybind11::module& m);
}
namespace robofish::mb_ba::PyAgent
{
	void bind(pybind11::module& m);
}
namespace robofish::mb_ba::PyCouzinAgent
{
	void bind(pybind11::module& m);
}
namespace robofish::mb_ba::PyMXNetAgent
{
	void bind(pybind11::module& m);
}

PYBIND11_MODULE(cpp, cpp)
{
	using namespace pybind11;
	using namespace pybind11::literals;
	using namespace robofish::mb_ba;
	using namespace robofish;

	PyRandomState::bind(cpp);

	PyIBody::bind(cpp);
	PyArrayBody::bind(cpp);
	PyAgent::bind(cpp);
	PyCouzinAgent::bind(cpp);
	PyMXNetAgent::bind(cpp);

	cpp.def(
	   "calc_locomotion",
	   [](array_t<float> poses_p_, array_t<float> poses_) {
		   if (poses_p_.ndim() == 1 && poses_.ndim() == 1) {
			   if (poses_p_.shape(0) == 4 && poses_.shape(0) == 4) {
				   auto poses_p = poses_p_.unchecked<1>();
				   auto poses   = poses_.unchecked<1>();
				   auto out_    = array_t<float>(std::vector<ssize_t>{2});
				   auto out     = out_.mutable_unchecked<1>();

				   std::array pose_p     = {poses_p(0), poses_p(1), poses_p(2), poses_p(3)};
				   std::array pose       = {poses(0), poses(1), poses(2), poses(3)};
				   auto       locomotion = calc_locomotion(pose_p, pose);
				   out(0)                = locomotion[0];
				   out(1)                = locomotion[1];
				   return out_;
			   }
		   } else if (poses_p_.ndim() == 2 && poses_.ndim() == 2) {
			   if (poses_p_.shape(0) == poses_.shape(0) && poses_p_.shape(1) == 4 && poses_.shape(1) == 4) {
				   auto poses_p = poses_p_.unchecked<2>();
				   auto poses   = poses_.unchecked<2>();
				   auto out_    = array_t<float>(std::vector<ssize_t>{poses_p_.shape(0), 2});
				   auto out     = out_.mutable_unchecked<2>();
				   for (ssize_t i = 0; i < poses_p.shape(0); ++i) {
					   std::array pose_p = {poses_p(i, 0), poses_p(i, 1), poses_p(i, 2), poses_p(i, 3)};
					   std::array pose   = {poses(i, 0), poses(i, 1), poses(i, 2), poses(i, 3)};

					   auto locomotion = calc_locomotion(pose_p, pose);
					   out(i, 0)       = locomotion[0];
					   out(i, 1)       = locomotion[1];
				   }
				   return out_;
			   }
		   }
		   throw std::invalid_argument("Arguments must have shape (4, ) or (_, 4)");
	   },
	   "poses_prev"_a,
	   "poses"_a);

	cpp.def("wall_distances",
	        vectorize([](interfaces::IBody& observer, stx::span<float> world, float ray_angle) {
		        return wall_distance(observer, {{0, 0}, {world[0], world[1]}}, ray_angle);
	        }),
	        "observer"_a,
	        "world"_a,
	        "rays"_a);

	cpp.def(
	   "centroid_distances",
	   [](interfaces::IBody&                              observer,
	      std::vector<std::shared_ptr<interfaces::IBody>> others,
	      stx::span<float>                                sector_limits) {
		   auto out = array_t<float>(sector_limits.size() - 1);
		   centroid_distances(stx::span<float>{out.mutable_data(), static_cast<std::size_t>(out.size())},
		                      observer,
		                      others,
		                      sector_limits);
		   return out;
	   },
	   "observer"_a,
	   "others"_a,
	   "sector_limits"_a);

	cpp.def("intensity_linear", vectorize(intensity_linear), "d"_a, "max_d"_a);
}
